<?php

namespace Drupal\pepper_graphql\Event;

use Drupal\Component\EventDispatcher\Event;

class ParagraphBundleMappingEvent extends Event
{
  /**
   * The paragraph bundle graphql mapping.
   *
   * @var array
   */
  protected $mapping;

  public function __construct(array $mapping) {
    $this->mapping = $mapping;
  }

  public function getMapping() {
    return $this->mapping;
  }

  public function setMapping(array $mapping) {
    $this->mapping = $mapping;
    return $mapping;
  }

}
