<?php

namespace Drupal\pepper_graphql\Event;

final class PepperGraphqlEvents
{
  /**
   * Event that is fired while the paragraph bundle mapping gets prepared.
   *
   * This is the chance for other modules to add/remove/change mappings.
   */
  const PARAGRAPH_BUNDLE_MAPPING = 'pepper_graphql.paragraph_bunlde_mapping';

  /**
   * Event that is fired while the content type bundle mapping gets prepared.
   *
   * This is the chance for other modules to add/remove/change mappings.
   */
  const CONTENT_TYPE_BUNDLE_MAPPING = 'pepper_graphql.content_type_bundle_mapping';
}
