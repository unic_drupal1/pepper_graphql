<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use GraphQL\Error\Error;

/**
* @SchemaExtension(
*   id = "menu",
*   name = "Menu",
*   description = "Provides the menu query over Graphql.",
*   schema = "custom_composable"
* )
*/
class MenuSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * Maps the schema languageId ENUM to Drupal language ids.
   *
   * Note! This is not the mapping to the path prefix itself. Here we only map
   * the incoming ENUM from GraphQL to the language id of Drupal.
   *
   * @var array
   *
   * @see \Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\RouteLoad::getPathPrefix
   */
  private $languageIdMapping = [
    'EN' => 'en',
    'DE' => 'de',
    'FR' => 'fr',
    'IT' => 'it',
  ];

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();
    $this->addMenuFields($registry, $builder);
  }

  public function addMenuFields(ResolverRegistryInterface $registry, ResolverBuilder $builder) {

    // Resolver for Menu query.
    $registry->addFieldResolver('Query', 'menu',
      $builder->produce('entity_load')
        ->map('type', $builder->fromValue('menu'))
        ->map('id', $builder->fromArgument('name'))
        ->map('access', $builder->fromValue(FALSE))
        ->map('language', $builder->callback(function ($value, $args, ResolveContext $context) {

          // Allow empty language parameter, as its optional.
          if (empty($args['language'])) {
            return '';
          }

          if (isset($this->languageIdMapping[$args['language']])) {
            $context->setContextLanguage($this->languageIdMapping[$args['language']]);
            return $this->languageIdMapping[$args['language']];
          }

          throw new Error('Could not resolve language.');

        }))
    );

    // Menu name.
    $registry->addFieldResolver('Menu', 'name',
      $builder->produce('pepper_graphql_menu_name')
        ->map('menu', $builder->fromParent())
    );

    // Menu items.
    $registry->addFieldResolver('Menu', 'items',
      $builder->compose(
        $builder->produce('menu_links')
          ->map('menu', $builder->fromParent()),
        $builder->produce('language_filter')
          ->map('entities', $builder->fromParent()),
        $builder->produce('access_filter')
          ->map('entities', $builder->fromParent())
      )
    );

    // Menu item id.
    $registry->addFieldResolver('MenuItem', 'id',
      $builder->produce('pepper_menu_link_id')
        ->map(
          'link', $builder->produce('menu_tree_link')
          ->map('element', $builder->fromParent())
        )
    );

    // Menu title.
    $registry->addFieldResolver('MenuItem', 'title',
      $builder->produce('menu_link_label')
        ->map('link', $builder->produce('menu_tree_link')
          ->map('element', $builder->fromParent())
        )
    );

    // Menu children.
    $registry->addFieldResolver('MenuItem', 'children',
      $builder->compose(
        $builder->produce('menu_tree_subtree')
          ->map('element', $builder->fromParent()),
        $builder->produce('language_filter')
          ->map('entities', $builder->fromParent()),
        $builder->produce('access_filter')
          ->map('entities', $builder->fromParent())
      )
    );

    // Menu url.
    $registry->addFieldResolver('MenuItem', 'url',
      $builder->produce('menu_link_url')
        ->map('link', $builder->produce('menu_tree_link')
          ->map('element', $builder->fromParent())
        )
    );

    $registry->addFieldResolver('Url', 'path',
      $builder->produce('url_path')
        ->map('url', $builder->fromParent())
    );

  }

}
