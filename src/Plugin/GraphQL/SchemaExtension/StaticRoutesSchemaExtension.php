<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
* @SchemaExtension(
*   id = "static_routes",
*   name = "Static routes",
*   schema = "custom_composable"
* )
*/
class StaticRoutesSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();
    $this->addStaticRoutes($registry, $builder);
  }

  /**
   * Add Static routes.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   */
  public function addStaticRoutes(ResolverRegistryInterface $registry, ResolverBuilder $builder) {

    $registry->addFieldResolver('Query', 'staticRoutes',
      $builder->compose(
        $builder->produce('pepper_route_items')
          ->map('type', $builder->fromValue('node')),
        $builder->produce('entity_load_multiple')
          ->map('ids', $builder->fromParent())
          ->map('type', $builder->fromValue('node')),
        $builder->produce('pepper_entity_translations')
          ->map('entities', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('RouteItem', 'path',
      $builder->compose(
        $builder->produce('entity_url')
          ->map('entity', $builder->fromParent()),
        $builder->produce('url_path')
          ->map('url', $builder->fromParent())
      )
    );

  }
}
