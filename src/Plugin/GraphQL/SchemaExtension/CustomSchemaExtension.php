<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Language\Language;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\PepperRouteLoad;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Schema extension.
 *
 * @SchemaExtension (
 *   id = "custom_schema",
 *   description = "Router and content_page - needs to get splitted.",
 *   name = "Custom schema",
 *   schema = "custom_composable"
 * )
 */
class CustomSchemaExtension extends SdlSchemaExtensionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Maps the schema languageId ENUM to Drupal language ids.
   *
   * Note! This is not the mapping to the path prefix itself. Here we only map
   * the incoming ENUM from GraphQL to the language id of Drupal.
   *
   * @var array
   *
   * @see PepperRouteLoad::getPathPrefix
   */
  private $languageIdMapping = [
    'EN' => 'en',
    'DE' => 'de',
    'FR' => 'fr',
    'IT' => 'it',
  ];

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('language_manager')
    );
  }

  /**
   * HeadlessSchema constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin Id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Cache\CacheBackendInterface $astCache
   *   The cache bin for caching the parsed SDL.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\graphql\Plugin\SchemaExtensionPluginManager $extensionManager
   *   The schema extension plugin manager.
   * @param array $config
   *   The service configuration.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    LanguageManagerInterface $languageManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler, $languageManager);
    $this->languageManager = $languageManager;
  }

  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();
    $this->addFieldResolvers($registry, $builder);
  }

  public function addFieldResolvers($registry, $builder) {
    $registry->addFieldResolver('Query', 'router',
      $builder->produce('pepper_route_load')
        ->map('path', $builder->fromArgument('path'))
        // Map the enum values from frontend to drupal langcodes.
        ->map('language', $builder->callback(function ($value, $args) {

          // Allow empty language parameter, as its optional.
          if (empty($args['language'])) {
            return '';
          }

          if (isset($this->languageIdMapping[$args['language']])) {
            return $this->languageIdMapping[$args['language']];
          }

          throw new Error('Could not resolve language.');

        }))
    );

    $registry->addFieldResolver('Query', 'preview',
      $builder->produce('pepper_preview_load')
        ->map('uuid', $builder->fromArgument('uuid'))
        // Map the enum values from frontend to drupal langcodes.
        ->map('language', $builder->callback(function ($value, $args) {

          // Allow empty language parameter, as its optional.
          if (empty($args['language'])) {
            return '';
          }

          if (isset($this->languageIdMapping[$args['language']])) {
            return $this->languageIdMapping[$args['language']];
          }

          throw new Error('Could not resolve language.');

        }))
    );

    $registry->addFieldResolver('ContentPage', 'nid',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ContentPage', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ContentPage', 'bundle',
      $builder->produce('entity_bundle')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ContentPage', 'title',
      $builder->produce('entity_label')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ContentPage', 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('ContentPage', 'date_published',
      $builder->produce('entity_created')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ContentPage', 'date_modified',
      $builder->produce('entity_changed')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ContentPage', 'lead',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:node'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_lead.value'))
    );

    $registry->addFieldResolver('ContentPage', 'teaser_title', $builder->compose(
      $builder->callback(function ($value) {
        // Return title if teaser title is not set.
        if ($value instanceof Node) {
          if ($value->hasField('field_teaser_title') && !empty($value->get('field_teaser_title')
              ->getValue())) {
            return $value->get('field_teaser_title')->value;
          }
          else {
            return $value->getTitle();
          }
        }
      }
      )));

    $registry->addFieldResolver('ContentPage', 'teaser_subtitle',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:node'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_teaser_subtitle.value'))
    );

    $registry->addFieldResolver('ContentPage', 'teaser_text', $builder->compose(
      $builder->callback(function ($value) {
        // Return lead if teaser text is not set.
        if ($value instanceof Node) {
          if ($value->hasField('field_teaser_text') && !empty($value->get('field_teaser_text')
              ->getValue())) {
            return $value->get('field_teaser_text')->value;
          }
          elseif ($value->hasField('field_lead') && !empty($value->get('field_lead')
              ->getValue())) {
            return $value->get('field_lead')->value;
          }
        }
      }
      )));

    $registry->addFieldResolver('ContentPage', 'teaser_image', $builder->compose(
      $builder->callback(function ($value) {
        // Return hero image if teaser image is not set.
        if ($value instanceof Node) {
          if ($value->hasField('field_teaser_image') && !empty($value->get('field_teaser_image')
              ->getValue())) {
            $mid = $value->get('field_teaser_image')
              ->getValue()['0']['target_id'];
          }
          elseif ($value->hasField('field_blog_hero') && !empty($value->get('field_blog_hero')
              ->getValue())) {
            $mid = $value->get('field_blog_hero')->getValue()['0']['target_id'];
          }
          if (isset($mid)) {
            $media = Media::load($mid);
            if ($media instanceof Media) {
              return $media;
            }
          }
        }
      }
      )));

    // Returns a single entity for field_image.
    $registry->addFieldResolver('ContentPage', 'image',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:node'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_image.entity'))
    );

    $registry->addFieldResolver('ContentPage', 'site_name',
      $builder->produce('site_name')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ContentPage', 'seo_information',
      $builder->produce('seo_information')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('metatags'))
    );

    $registry->addFieldResolver('ContentPage', 'breadcrumb',
        $builder->produce('pepper_breadcrumb')
          ->map('entity', $builder->fromParent())
          ->map('language', $builder->fromContext('language'))
    );

    $registry->addFieldResolver('ContentPage', 'langcode',
      $builder->compose(
        $builder->produce('entity_language')->map('entity', $builder->fromParent()),
        $builder->callback(function (Language $parent) {
          return $parent->getId();
        })
    ));

    $registry->addFieldResolver('ContentPage', 'url',
      $builder->compose(
        $builder->produce('entity_url')
          ->map('entity', $builder->fromParent()),
        $builder->produce('url_path')
          ->map('url', $builder->fromParent())
      )
    );

    $this->addImageMediaFields($registry, $builder);
    $this->addSvgMediaFields($registry, $builder);
    $this->addVideoMediaFields($registry, $builder);
    $this->addRemoteVideoMediaFields($registry, $builder);
    $this->addAudioMediaFields($registry, $builder);
    $this->addDocumentMediaFields($registry, $builder);
    $this->addLinkFields($registry, $builder);
    $this->addSeoInformationFields($registry, $builder);
    $this->addAlternatePathItemFields($registry, $builder);
    $this->addFieldResolversTaxonomyTerms($registry, $builder);
    $this->addArticlePageFields($registry, $builder);
  }

  /**
   * Adds the image media fields that are needed for all queries.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addImageMediaFields(ResolverRegistry $registry, ResolverBuilder $builder) {
    // Image media.
    $imageProperties = [
      'title','alt', 'width', 'height', 'type', 'src',
    ];
    foreach ($imageProperties as $imageProperty) {
      $registry->addFieldResolver('ImageMedia', $imageProperty,
        $builder->produce('media_entity_image')
          ->map('input', $builder->fromParent())
          ->map('info', $builder->fromValue($imageProperty))
          ->map('language', $builder->fromContext('language'))
      );
    }
    $registry->addFieldResolver('ImageMedia', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ImageMedia', 'media_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    // This must be called separately because of the argument.
    $registry->addFieldResolver('ImageMedia', 'fallback',
      $builder->produce('media_entity_image_responsive_fallback')
        ->map('input', $builder->fromParent())
        ->map('style', $builder->fromArgument('style'))
    );

    $registry->addFieldResolver('ImageMedia', 'image_style',
      $builder->produce('media_entity_image_style')
        ->map('entity', $builder->fromParent())
        ->map('style', $builder->fromArgument('style'))
    );

    $registry->addFieldResolver('ImageMedia', 'sources',
      $builder->produce('media_entity_image_responsive')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('sources'))
        ->map('style', $builder->fromArgument('style'))
    );

    $imageSourcesProperties = ['src', 'media', 'sizes'];
    foreach ($imageSourcesProperties as $property) {
      $registry->addFieldResolver('ImageSource', $property,
        $builder->produce('media_entity_image_sources')
          ->map('input', $builder->fromParent())
          ->map('info', $builder->fromValue($property))
      );
    }

    $registry->addFieldResolver('ImageMedia', 'caption',
      $builder->produce('pepper_graphql_property_path')
        ->map('type', $builder->fromValue('entity:media'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_caption.value'))
    );
  }

  /**
   * Adds the svg media fields that are needed for all queries.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addSvgMediaFields(ResolverRegistry $registry, ResolverBuilder $builder) {
    // Svg media.
    $imageProperties = [
      'alt', 'type', 'src',
    ];
    foreach ($imageProperties as $imageProperty) {
      $registry->addFieldResolver('SvgMedia', $imageProperty,
        $builder->produce('media_entity_image')
          ->map('input', $builder->fromParent())
          ->map('info', $builder->fromValue($imageProperty))
      );
    }
    $registry->addFieldResolver('SvgMedia', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('SvgMedia', 'media_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('SvgMedia', 'title',
      $builder->produce('entity_label')
        ->map('entity', $builder->fromParent())
    );

  }

  /**
   * Adds the video media fields that are needed for all queries.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addVideoMediaFields(ResolverRegistry $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('VideoMedia', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('VideoMedia', 'media_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('VideoMedia', 'src',
      $builder->produce('media_entity_video')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('src'))
    );

    $registry->addFieldResolver('VideoMedia', 'caption',
        $builder->produce('property_path')
            ->map('type', $builder->fromValue('entity:media'))
            ->map('value', $builder->fromParent())
            ->map('path', $builder->fromValue('field_caption.value'))
    );
  }

  /**
   * Adds the remote video media fields that are needed for all queries.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addRemoteVideoMediaFields(ResolverRegistry $registry, ResolverBuilder $builder) {

    $registry->addFieldResolver('RemoteVideoMedia', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('RemoteVideoMedia', 'media_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('RemoteVideoMedia', 'src',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:media'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_media_oembed_video.value'))
    );

    $registry->addFieldResolver('RemoteVideoMedia', 'video_id',
      $builder->produce('video_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('RemoteVideoMedia', 'video_provider',
      $builder->produce('video_provider')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('RemoteVideoMedia', 'caption',
        $builder->produce('property_path')
            ->map('type', $builder->fromValue('entity:media'))
            ->map('value', $builder->fromParent())
            ->map('path', $builder->fromValue('field_caption.value'))
    );
  }

  /**
   * Adds the audio media fields that are needed for all queries.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addAudioMediaFields(ResolverRegistry $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('AudioMedia', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('AudioMedia', 'media_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('AudioMedia', 'src',
      $builder->produce('media_entity_audio')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('src'))
    );

    $registry->addFieldResolver('AudioMedia', 'caption',
        $builder->produce('property_path')
            ->map('type', $builder->fromValue('entity:media'))
            ->map('value', $builder->fromParent())
            ->map('path', $builder->fromValue('field_caption.value'))
    );
  }

  /**
   * Adds the document media fields that are needed for all queries.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addDocumentMediaFields(ResolverRegistry $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('DocumentMedia', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('DocumentMedia', 'media_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('DocumentMedia', 'title',
      $builder->produce('entity_label')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('DocumentMedia', 'src',
      $builder->produce('media_entity_document')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('src'))
    );

    $registry->addFieldResolver('DocumentMedia', 'mimetype',
      $builder->produce('media_entity_document')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('mimetype'))
    );

    $registry->addFieldResolver('DocumentMedia', 'extension',
      $builder->produce('media_entity_document')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('extension'))
    );

    $registry->addFieldResolver('DocumentMedia', 'filesize',
      $builder->produce('media_entity_document')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('filesize'))
    );

    $registry->addFieldResolver('DocumentMedia', 'caption',
        $builder->produce('property_path')
            ->map('type', $builder->fromValue('entity:media'))
            ->map('value', $builder->fromParent())
            ->map('path', $builder->fromValue('field_caption.value'))
    );
  }

  /**
   * Adds the link fields that are needed for all queries.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addLinkFields(ResolverRegistry $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('Link', 'url',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_link.url'))
    );

    $registry->addFieldResolver('Link', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_link.title'))
    );
  }

  /**
   * Adds the seo information fields that are needed for all content types.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addSeoInformationFields(ResolverRegistry $registry, ResolverBuilder $builder) {
    $seoProperties = [
      'meta_description',
      'page_title',
      'canonical',
      'meta_name_robots',
      'keywords',
      'hreflang',
    ];
    foreach ($seoProperties as $seoProperty) {
      $registry->addFieldResolver('SeoInformation', $seoProperty,
        $builder->produce('seo_information')
          ->map('input', $builder->fromParent())
          ->map('info', $builder->fromValue($seoProperty))
      );
    }
  }

  /**
   * Adds the content fields that are needed to the contentPage query.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addArticlePageFields(ResolverRegistry $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('ContentPage', 'alternates',
      $builder->produce('pepper_graphql_entity_translations')
        ->map('entity', $builder->fromParent())
    );
  }

  /**
   * Adds the content element fields that are needed for all queries.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addAlternatePathItemFields(ResolverRegistryInterface $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('AlternatePathItem', 'language_id', $builder->compose(
      $builder->produce('entity_language')->map('entity', $builder->fromParent()),
      $builder->callback(function (Language $parent) {
        $map = array_flip($this->languageIdMapping);
        return $map[$parent->getId()];
      })
    ));

    $registry->addFieldResolver('AlternatePathItem', 'url', $builder->compose(
      $builder->produce('entity_url_save')->map('entity', $builder->fromParent()),
      $builder->produce('url_path')->map('url', $builder->fromParent())
    ));
  }


  /**
   * Adds all fields for taxonomy terms.
   *
   * @param $registry
   *   The registry of the resolvers.
   * @param $builder
   *   The builder of the resolvers.
   * @return void
   *
   */
  public function addFieldResolversTaxonomyTerms($registry, $builder): void {
    $registry->addFieldResolver('TaxonomyTerm', 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TaxonomyTerm', 'name',
      $builder->produce('entity_label')
        ->map('entity', $builder->fromParent())
    );
  }
}
