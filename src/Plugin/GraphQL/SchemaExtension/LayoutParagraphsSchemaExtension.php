<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\pepper_graphql\Event\ParagraphBundleMappingEvent;
use Drupal\pepper_graphql\Event\PepperGraphqlEvents;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @SchemaExtension (
 *   id = "layout_paragraphs",
 *   name = "Layout paragraphs",
 *   description = "Layout paragraphs schema extension.",
 *   schema = "custom_composable"
 * )
 */
class LayoutParagraphsSchemaExtension extends SdlSchemaExtensionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Maps the schema languageId ENUM to Drupal language ids.
   *
   * Note! This is not the mapping to the path prefix itself. Here we only map * the incoming ENUM from GraphQL to the language id of Drupal.
   *
   * @var array
   *
   * @see PepperRouteLoad::getPathPrefix
   */
  private $languageIdMapping = [
    'EN' => 'en',
    'DE' => 'de',
    'FR' => 'fr',
    'IT' => 'it',
  ];

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('language_manager'),
      $container->get('event_dispatcher'));
  }

  /**
   * HeadlessSchema constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin Id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Cache\CacheBackendInterface $astCache
   *   The cache bin for caching the parsed SDL.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param array $config
   *   The service configuration.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service .
   */
  public function __construct(
    array $configuration,
          $pluginId,
          $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    LanguageManagerInterface $languageManager,
    EventDispatcherInterface $eventDispatcher
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler, $languageManager, $eventDispatcher);
    $this->languageManager = $languageManager;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   *
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();
    $registry->addFieldResolver('ContentPage', 'content_elements', $builder->compose(
      $builder->callback(function ($values) {
        if ($values instanceof Node) {
          $nodeLanguage = $values->language()->getId();
          if ($values->hasField('field_layout_paragraphs') && $paragraphs = $values->get('field_layout_paragraphs')->referencedEntities()) {
            $returned = [];
            /** @var \Drupal\paragraphs\Entity\ParagraphInterface $paragraph */
            foreach ($paragraphs as $paragraph) {
              if (!$paragraph->isPublished()) {
                continue;
              }
              if ($paragraph->hasTranslation($nodeLanguage)) {
                /** @var \Drupal\paragraphs\Entity\ParagraphInterface $translated_paragraph */
                $translated_paragraph = $paragraph->getTranslation($nodeLanguage);
                if (!$translated_paragraph->isPublished()) {
                  continue;
                }
                $returned[$translated_paragraph->getRevisionId()] = $translated_paragraph;
              }
              else {
                $returned[$paragraph->getRevisionId()] = $paragraph;
              }
            }
            return $returned;
          }
        }
        return [];
      }),
      $builder->callback(function ($values) {
        return array_filter($values, function ($paragraph) {
          /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
          $parent_uuid = $paragraph->getBehaviorSetting('layout_paragraphs', 'parent_uuid');
          return empty($parent_uuid);
        });
      })
    ));

    $registry->addFieldResolver('ContentPage', 'content_elements__preview', $builder->compose(
      $builder->callback(function ($values) {
        if ($values instanceof Node) {
          if ($serialized = \Drupal::service('tempstore.shared')->get('decoupled_preview')->get($values->uuid())) {
            /** @var \Drupal\node\Entity\NodeInterface $values */
            $values = unserialize($serialized);
            $nodeLanguage = $values->language()->getId();
            if ($values->hasField('field_layout_paragraphs') && $paragraphs = $values->get('field_layout_paragraphs')->getValue()) {
              $paragraphs = array_column($paragraphs, 'entity');

              /**
               * @var int $index
               * @var \Drupal\paragraphs\Entity\ParagraphInterface $paragraph
               */
              foreach ($paragraphs as $index => $paragraph) {
                if ($paragraph->hasTranslation($nodeLanguage)) {
                  $paragraphs[$index] = $paragraph->getTranslation($nodeLanguage);
                }
              }
              return $paragraphs;
            }
          }
        }
        return [];
      }),
      $builder->callback(function ($values) {
        return array_filter($values, function ($paragraph) {
          /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
          $parent_uuid = $paragraph->getBehaviorSetting('layout_paragraphs', 'parent_uuid');
          return empty($parent_uuid);
        });
      })
    ));

    $this->addFieldResolvers($registry, $builder);
  }

  /**
   *
   */
  public function addFieldResolvers($registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('Layout', 'regions',
      $builder->produce('layout_paragraphs_regions')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('Layout', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('Layout', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('Layout', 'name',
      $builder->produce('layout_paragraphs_layout')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('Region', 'name',
      $builder->produce('layout_paragraphs_paragraphs')
        ->map('input', $builder->fromParent())
        ->map('position', $builder->fromValue(0))
        ->map('info', $builder->fromValue('name'))
    );

    $registry->addFieldResolver('Region', '_id',
      $builder->produce('layout_paragraphs_paragraphs')
        ->map('input', $builder->fromParent())
        ->map('position', $builder->fromValue(0))
        ->map('info', $builder->fromValue('uuid'))
    );

    $registry->addFieldResolver('Region', 'paragraphs',
      $builder->produce('layout_paragraphs_paragraphs')
        ->map('input', $builder->fromParent())
        ->map('position', $builder->fromValue(0))
        ->map('info', $builder->fromValue('paragraphs'))
    );

    $registry->addFieldResolver('ParagraphInterface', 'type',
      $builder->produce('entity_bundle')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ParagraphInterface', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ParagraphInterface', 'langcode',
      $builder->compose(
        $builder->produce('entity_language')
          ->map('entity', $builder->fromParent()),
        $builder->callback(function (Language $parent) {
          $map = array_flip($this->languageIdMapping);
          return $map[$parent->getId()];
        })
      )
    );

    $registry->addTypeResolver('ParagraphInterface', function ($value) {
      $paragraphBundleMapping = $this->getParagraphBundleMapping();
      if ($value instanceof Paragraph) {
        if (isset($paragraphBundleMapping[$value->bundle()])) {
          return $paragraphBundleMapping[$value->bundle()];
        }
      }
      throw new Error('Could not resolve ParagraphInterface type of bundle ' . $value->bundle() . '.');
    });

    $this->addTextItemFields($registry, $builder);
    $this->addTableItemFields($registry, $builder);
    $this->addVideoItemFields($registry, $builder);
    $this->addImageItemFields($registry, $builder);
    $this->addAudioItemFields($registry, $builder);
    $this->addSlideshowItemFields($registry, $builder);
    $this->addAccordionItemFields($registry, $builder);
    $this->addQuoteItemFields($registry, $builder);
    $this->addLinkListItemFields($registry, $builder);
    $this->addDownloadListItemFields($registry, $builder);
    $this->addContentTeaserItemFields($registry, $builder);
    $this->addTeaserInternalItemFields($registry, $builder);
    $this->addTeaserExternalItemFields($registry, $builder);
    $this->addTeaserListItemFields($registry, $builder);
    $this->addCardItemFields($registry, $builder);
    $this->addChartItemFields($registry, $builder);
    $this->addMapItemFields($registry, $builder);
    $this->addTabItemFields($registry, $builder);
    $this->addLibraryItemFields($registry, $builder);
  }

  /**
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addVideoItemFields($registry, $builder): void {
    $registry->addFieldResolver('VideoItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('VideoItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('VideoItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('VideoItem', 'caption',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_caption.value'))
    );

    $registry->addFieldResolver('VideoItem', 'video',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_video.entity'))
    );

    $registry->addFieldResolver('VideoItem', 'autostart',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_autostart.value'))
    );

    $registry->addFieldResolver('VideoItem', 'mute',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_mute.value'))
    );

    $registry->addFieldResolver('VideoItem', 'loop',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_loop.value'))
    );

    $registry->addFieldResolver('VideoItem', 'controls',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_controls.value'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addAudioItemFields($registry, $builder): void {
    $registry->addFieldResolver('AudioItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('AudioItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('AudioItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('AudioItem', 'caption',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_caption.value'))
    );

    $registry->addFieldResolver('AudioItem', 'audio',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_audio.entity'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addImageItemFields($registry, $builder): void {
    $registry->addFieldResolver('ImageItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ImageItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ImageItem', 'image',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_image.entity'))
    );

    $registry->addFieldResolver('ImageItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('ImageItem', 'caption',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_caption.value'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addTextItemFields($registry, $builder): void {
    $registry->addFieldResolver('TextItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TextItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TextItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('TextItem', 'text',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_text.processed'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addMapItemFields($registry, $builder): void {
    $registry->addFieldResolver('MapItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('MapItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('MapItem', 'latitude',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_latitude.value'))
    );

    $registry->addFieldResolver('MapItem', 'longitude',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_longitude.value'))
    );

    $registry->addFieldResolver('MapItem', 'maxzoom',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_maxzoom.value'))
    );

    $registry->addFieldResolver('MapItem', 'mapzoom',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_mapzoom.value'))
    );

    $registry->addFieldResolver('MapItem', 'attribution',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_attribution.value'))
    );

    $registry->addFieldResolver('MapItem', 'iconpopup',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_iconpopup.value'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addTableItemFields($registry, $builder): void {
    $registry->addFieldResolver('TableItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TableItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TableItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('TableItem', 'text',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_text.processed'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addSlideshowItemFields($registry, $builder): void {
    $registry->addFieldResolver('SlideshowItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('SlideshowItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('SlideshowItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('SlideshowItem', 'medias',
      $builder->produce('entity_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_media'))
        ->map('language', $builder->fromContext('language'))
    );

    $registry->addFieldResolver('SlideshowItem', 'loop',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_loop.value'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addTabItemFields($registry, $builder): void {
    $registry->addFieldResolver('TabItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TabItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TabItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('TabItem', 'singleItemExpansion',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_single_item_expansion.value'))
    );

    $registry->addFieldResolver('TabItem', 'elements',
      $builder->produce('pepper_entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_tab_elements'))
    );

    $registry->addFieldResolver('TabElementItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TabElementItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TabElementItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('TabElementItem', 'expanded',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_expanded.value'))
    );

    $registry->addFieldResolver('TabElementItem', 'content_elements',
      $builder->produce('pepper_entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_paragraphs'))
    );

  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addAccordionItemFields($registry, $builder): void {
    $registry->addFieldResolver('AccordionItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('AccordionItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('AccordionItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('AccordionItem', 'singleItemExpansion',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_single_item_expansion.value'))
    );

    $registry->addFieldResolver('AccordionItem', 'elements',
      $builder->produce('pepper_entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_accordion_elements'))
    );

    $registry->addFieldResolver('AccordionElementItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('AccordionElementItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('AccordionElementItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('AccordionElementItem', 'expanded',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_expanded.value'))
    );

    $registry->addFieldResolver('AccordionElementItem', 'content_elements',
      $builder->produce('pepper_entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_paragraphs'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addQuoteItemFields($registry, $builder): void {
    $registry->addFieldResolver('QuoteItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('QuoteItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('QuoteItem', 'text',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_text.processed'))
    );

    $registry->addFieldResolver('QuoteItem', 'author',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_author.value'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addLinkListItemFields($registry, $builder): void {
    $registry->addFieldResolver('LinkListItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('LinkListItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('LinkListItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('LinkListItem', 'links',
      $builder->callback(function (ParagraphInterface $paragraph) {
        return $paragraph->field_links->getValue();
      })
    );

    $registry->addFieldResolver('Link', 'title',
      $builder->produce('pepper_link_title')
        ->map('link', $builder->fromParent())
        ->map('language', $builder->fromContext('language'))
    );

    $registry->addFieldResolver('Link', 'target',
      $builder->produce('pepper_link_target')
        ->map('link', $builder->fromParent())
        ->map('language', $builder->fromContext('language'))
    );

    $registry->addFieldResolver('Link', 'url',
      $builder->produce('pepper_link_url')
        ->map('link', $builder->fromParent())
        ->map('language', $builder->fromContext('language'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addDownloadListItemFields($registry, $builder): void {
    $registry->addFieldResolver('DownloadListItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('DownloadListItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('DownloadListItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('DownloadListItem', 'downloads',
      $builder->produce('entity_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_documents'))
        ->map('language', $builder->fromContext('language'))
    );
  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addContentTeaserItemFields($registry, $builder): void {
    $registry->addFieldResolver('ContentTeaserItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ContentTeaserItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    // Returns field_title or field_teaser_title from target node.
    $registry->addFieldResolver('ContentTeaserItem', 'title',
      $builder->produce('pepper_content_teaser_item_text')
        ->map('entity', $builder->fromParent())
        ->map('language', $builder->fromContext('language'))
        ->map('field_paragraph', $builder->fromValue('field_title'))
        ->map('field_node', $builder->fromValue('field_teaser_title'))
    );

    // Returns field_subtitle or field_teaser_subtitle from target node.
    $registry->addFieldResolver('ContentTeaserItem', 'subtitle',
      $builder->produce('pepper_content_teaser_item_text')
        ->map('entity', $builder->fromParent())
        ->map('language', $builder->fromContext('language'))
        ->map('field_paragraph', $builder->fromValue('field_subtitle'))
        ->map('field_node', $builder->fromValue('field_teaser_subtitle'))
    );

    // Returns field_text or field_teaser_text from target node.
    $registry->addFieldResolver('ContentTeaserItem', 'teaser_text',
      $builder->produce('pepper_content_teaser_item_text')
        ->map('entity', $builder->fromParent())
        ->map('language', $builder->fromContext('language'))
        ->map('field_paragraph', $builder->fromValue('field_text'))
        ->map('field_node', $builder->fromValue('field_teaser_text'))
    );

    // Returns a single entity for field_image or teaser_image from target node.
    $registry->addFieldResolver('ContentTeaserItem', 'image',
      $builder->compose(
        $builder->produce('pepper_content_teaser_item_media')
          ->map('entity', $builder->fromParent())
          ->map('language', $builder->fromContext('language'))
          ->map('field_paragraph', $builder->fromValue('field_image'))
          ->map('field_node', $builder->fromValue('field_teaser_image')),
      )
    );

    $registry->addFieldResolver('ContentTeaserItem', 'url',
      $builder->callback(function (ParagraphInterface $paragraph) {
        if ($links = $paragraph->field_link->getValue()) {
          $uri = isset($links[0]['uri']) ? $links[0]['uri'] : NULL;
          if ($uri) {
            return Url::fromUri($uri)->toString(TRUE)->getGeneratedUrl();
          }
        }
        return NULL;
      })
    );

    $registry->addFieldResolver('ContentTeaserItem', 'link_text',
      $builder->callback(function (ParagraphInterface $paragraph) {
        if ($links = $paragraph->field_link->getValue()) {
          return isset($links[0]['title']) ? $links[0]['title'] : NULL;
        }
        return NULL;
      })
    );
  }

  /**
   * Add TeaserInternal Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addTeaserInternalItemFields($registry, $builder): void {
    $registry->addFieldResolver('TeaserInternalItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TeaserInternalItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TeaserInternalItem', 'title',
      $builder->produce('pepper_graphql_teaser_overwrite')
        ->map('entity', $builder->fromParent())
        ->map('overwrite_teaser_field', $builder->fromValue('title'))
        ->map('target_node_field', $builder->fromValue('field_target_node'))
    );

    $registry->addFieldResolver('TeaserInternalItem', 'text',
      $builder->produce('pepper_graphql_teaser_overwrite')
        ->map('entity', $builder->fromParent())
        ->map('overwrite_teaser_field', $builder->fromValue('text'))
        ->map('target_node_field', $builder->fromValue('field_target_node'))
    );

    $registry->addFieldResolver('TeaserInternalItem', 'image',
      $builder->produce('pepper_graphql_teaser_overwrite')
        ->map('entity', $builder->fromParent())
        ->map('overwrite_teaser_field', $builder->fromValue('image'))
        ->map('target_node_field', $builder->fromValue('field_target_node'))
    );

    $registry->addFieldResolver('TeaserInternalItem', 'link_name',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_link_name.value'))
    );

    $registry->addFieldResolver('TeaserInternalItem', 'target_node',
      $builder->compose(
        $builder->produce('entity_reference')
          ->map('entity', $builder->fromParent())
          ->map('field', $builder->fromValue('field_target_node'))
          ->map('language', $builder->fromContext('language')),
        $builder->produce('seek')
          ->map('input', $builder->fromParent())
          ->map('position', $builder->fromValue(0))
      )
    );

  }

  /**
   * Add Teaser External Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addTeaserExternalItemFields($registry, $builder): void {
    $registry->addFieldResolver('TeaserExternalItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TeaserExternalItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    // Returns field_title or field_teaser_title from target node.
    $registry->addFieldResolver('TeaserExternalItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value')),
    );

    $registry->addFieldResolver('TeaserExternalItem', 'text',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_text.value')),
    );

    $registry->addFieldResolver('TeaserExternalItem', 'image',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_image.entity'))
    );

    $registry->addFieldResolver('TeaserExternalItem', 'link',
      $builder->callback(function (ParagraphInterface $paragraph) {
        return $paragraph->field_link->getValue();
      })
    );

  }

  /**
   * Add Paragraph field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addTeaserListItemFields($registry, $builder): void {
    $registry->addFieldResolver('TeaserListItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TeaserListItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TeaserListItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('TeaserListItem', 'teasers',
      $builder->produce('entity_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_teasers'))
        ->map('language', $builder->fromContext('language'))
    );

    $registry->addFieldResolver('TeaserListItem', 'list_type',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_type.value'))
    );

    $registry->addTypeResolver('TeaserItemTypes', function ($value) {

      if ($value instanceof ParagraphInterface) {
        switch ($value->bundle()) {

          case 'content_teaser':
            return 'ContentTeaserItem';

          case 'teaser_external':
            return 'TeaserExternalItem';

          case 'teaser_internal':
            return 'TeaserInternalItem';

        }
        throw new Error('Could not resolve content type. ' . $value->bundle());
      }
    });
  }

  /**
   * Add Card field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addCardItemFields($registry, $builder): void {
    // Fields for a single card.
    $registry->addFieldResolver('CardItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('CardItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );
    $registry->addFieldResolver('CardItem', 'image',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_image.entity'))
    );

    $registry->addFieldResolver('CardItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('CardItem', 'text',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_text.value'))
    );

    $registry->addFieldResolver('CardItem', 'cta',
      $builder->callback(function (ParagraphInterface $paragraph) {
        if ($firstLink = $paragraph->field_link->first()) {
          return $firstLink->getValue();
        }
      })
    );

    // Fields for the card section.
    $registry->addFieldResolver('CardSectionItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('CardSectionItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('CardSectionItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('CardSectionItem', 'variant',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_variant.value'))
    );

    $registry->addFieldResolver('CardSectionItem', 'cards',
      $builder->produce('pepper_entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_cards'))
    );
  }

  /**
   * Add Paragraphs Library field resolver.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   GraphQL resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   GraphQL resolver builder.
   */
  protected function addChartItemFields($registry, $builder): void {

    $registry->addFieldResolver('ChartItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );
    $registry->addFieldResolver('ChartItem', '_id',
    $builder->produce('entity_uuid')
      ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ChartItem', 'chart_title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_chart_title.value'))
    );

    $registry->addFieldResolver('ChartItem', 'xaxis',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_xaxis.value'))
    );

    $registry->addFieldResolver('ChartItem', 'yaxis',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_yaxis.value'))
    );

    $registry->addFieldResolver('ChartItem', 'chart_data',
      $builder->produce('entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_chart_data'))
    );

    $registry->addFieldResolver('ChartDataItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ChartDataItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ChartDataItem', 'name',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_name.value'))
    );

    $registry->addFieldResolver('ChartDataItem', 'data',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_data.value'))
    );

    $registry->addFieldResolver('ChartDataItem', 'chart_type',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_chart_type.value'))
    );

    $registry->addFieldResolver('ChartDataItem', 'content_elements',
      $builder->produce('entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_paragraphs'))
    );
  }

  /**
   *
   */
  protected function addLibraryItemFields($registry, $builder): void {
    $registry->addFieldResolver('LibraryItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('LibraryItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );
    $registry->addFieldResolver('LibraryItem', 'paragraph',
      $builder->produce('layout_paragraphs_library_item')
        ->map('entity', $builder->fromParent())
    );
  }

  /**
   *
   */
  protected function getParagraphBundleMapping() {
    $mapping = [
      'layout_section' => 'Layout',
      'text' => 'TextItem',
      'table' => 'TableItem',
      'image' => 'ImageItem',
      'video' => 'VideoItem',
      'audio' => 'AudioItem',
      'slideshow' => 'SlideshowItem',
      'accordion' => 'AccordionItem',
      'quote' => 'QuoteItem',
      'link_list' => 'LinkListItem',
      'download_list' => 'DownloadListItem',
      'headline' => 'HeadlineItem',
      'form' => 'WebformItem',
      'content_teaser' => 'ContentTeaserItem',
      'teaser_list' => 'TeaserListItem',
      'card' => 'CardItem',
      'card_section' => 'CardSectionItem',
      'charts' => 'ChartItem',
      'map' => 'MapItem',
      'tab' => 'TabItem',
      'from_library' => 'LibraryItem',
      'teaser_external' => 'TeaserExternalItem',
      'teaser_internal' => 'TeaserInternalItem',
    ];

    // Give other modules the chance to alter the mapping, in case there are new paragraph types that are not
    // implemented in pepper.
    $mappingEvent = new ParagraphBundleMappingEvent($mapping);
    $this->eventDispatcher->dispatch($mappingEvent, PepperGraphqlEvents::PARAGRAPH_BUNDLE_MAPPING);

    return $mappingEvent->getMapping();
  }

}
