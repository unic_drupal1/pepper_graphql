<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
 * @SchemaExtension (
 *   id = "site_settings",
 *   name = "Site settings",
 *   description = "Provides the site settings query over Graphql.",
 *   schema = "custom_composable"
 * )
 */
class SiteSettingsSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();
    $this->addSettingsFields($registry, $builder);
  }

  /**
   * Adds the fields to the schema.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  public function addSettingsFields(ResolverRegistryInterface $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('Query', 'siteSettings',
      $builder->produce('pepper_site_settings')
        ->map('language', $builder->fromArgument('language'))
    );
  }

}
