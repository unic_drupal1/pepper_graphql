<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\Schema;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\SchemaExtensionPluginManager;
use Drupal\media\MediaInterface;
use Drupal\pepper_graphql\Event\ContentTypeBundleMappingEvent;
use Drupal\pepper_graphql\Event\PepperGraphqlEvents;
use Drupal\pepper_graphql\Wrapper\Routing\EntityResponse;
use Drupal\pepper_graphql\Wrapper\Routing\ErrorResponse;
use Drupal\pepper_graphql\Wrapper\Routing\RedirectResponse;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Drupal\graphql\Plugin\GraphQL\Schema\ComposableSchema;
use Drupal\node\NodeInterface;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Schema.
 *
 * @Schema(
 *   id = "custom_composable",
 *   name = "Custom composable schema",
 *   extensions = "composable"
 * )
 */
class CustomComposableSchema extends ComposableSchema implements ContainerFactoryPluginInterface {

  /**
   * The event dispatcher.
   *
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('cache.graphql.ast'),
      $container->get('module_handler'),
      $container->get('plugin.manager.graphql.schema_extension'),
      $container->getParameter('graphql.config'),
      $container->get('language_manager'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * CustomComposableSchema constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin Id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Cache\CacheBackendInterface $astCache
   *   The cache bin for caching the parsed SDL.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\graphql\Plugin\SchemaExtensionPluginManager $extensionManager
   *   The schema extension plugin manager.
   * @param array $config
   *   The service configuration.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service
   */
  public function __construct(
    array $configuration,
          $pluginId,
    array $pluginDefinition,
    CacheBackendInterface $astCache,
    ModuleHandlerInterface $moduleHandler,
    SchemaExtensionPluginManager $extensionManager,
    array $config,
    LanguageManagerInterface $languageManager,
    EventDispatcherInterface $eventDispatcher
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $astCache, $moduleHandler, $extensionManager, $config);
    $this->languageManager = $languageManager;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function getResolverRegistry() {
    $registry = new ResolverRegistry([get_class($this), 'defaultFieldResolver']);

    $registry->addTypeResolver('NodeInterface', function ($value) {
      $contentTypeBundleMapping = $this->getContentTypeBundleMapping();
      if ($value instanceof NodeInterface) {
        if (isset($contentTypeBundleMapping[$value->bundle()])) {
          return $contentTypeBundleMapping[$value->bundle()];
        }
      }
      throw new Error('Could not resolve content type of bundle ' . $value->bundle() . '.');
    });

    // ImageVideo is the union for Image- and Videomedia
    $registry->addTypeResolver('Video', function ($value) {
      if ($value instanceof MediaInterface) {
        switch ($value->bundle()) {
          case 'video':
            return 'VideoMedia';

          case 'remote_video':
            return 'RemoteVideoMedia';
        }
        throw new Error('Could not resolve content type. ' . $value->bundle());
      }
    });

    // ImageVideo is the union for Image- and Videomedia
    $registry->addTypeResolver('ImageVideo', function ($value) {
      if ($value instanceof MediaInterface) {
        switch ($value->bundle()) {
          case 'image':
            return 'ImageMedia';

          case 'svg':
            return 'SvgMedia';

          case 'video':
            return 'VideoMedia';

          case 'remote_video':
            return 'RemoteVideoMedia';
        }
        throw new Error('Could not resolve content type. ' . $value->bundle());
      }
    });

    // ImageVideoAudio is the union for Image-, Video- and Audiomedia
    $registry->addTypeResolver('ImageVideoAudio', function ($value) {
      if ($value instanceof MediaInterface) {
        switch ($value->bundle()) {
          case 'image':
            return 'ImageMedia';

          case 'svg':
            return 'SvgMedia';

          case 'video':
            return 'VideoMedia';

          case 'remote_video':
            return 'RemoteVideoMedia';

          case 'audio':
            return 'audio';
        }
        throw new Error('Could not resolve content type. ' . $value->bundle());
      }
    });

    // ImageVideoAudio is the union for Image-, Video- and Audiomedia
    $registry->addTypeResolver('Image', function ($value) {
      if ($value instanceof MediaInterface) {
        switch ($value->bundle()) {
          case 'image':
            return 'ImageMedia';

          case 'svg':
            return 'SvgMedia';
        }
        throw new Error('Could not resolve content type. ' . $value->bundle());
      }
    });

    $registry->addTypeResolver('RouteResponseInterface', function ($value) {
      switch (TRUE) {
        case ($value instanceof EntityResponse):
          return 'EntityResponse';

        case ($value instanceof RedirectResponse):
          return 'RedirectResponse';

        case ($value instanceof ErrorResponse):
          return 'ErrorResponse';
      }

      throw new Error('Could not resolve response type: ' . $value);
    });

    return $registry;
  }

  /**
   * The default field resolver.
   *
   * Used if no field resolver was explicitly registered.
   *
   * @param mixed $source
   *   The source (parent) value.
   * @param array $args
   *   An array of arguments.
   * @param $context
   *   The context object.
   * @param \GraphQL\Type\Definition\ResolveInfo $info
   *   The resolve info object.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The field context.
   *
   * @return array|mixed|null
   *   The result for the field.
   */
  public static function defaultFieldResolver($source, array $args, $context, ResolveInfo $info, FieldContext $fieldContext) {
    $fieldName = $info->fieldName;
    $property = NULL;
    if (is_array($source) || $source instanceof \ArrayAccess) {
      if (isset($source[$fieldName])) {
        $property = $source[$fieldName];
      }
    }
    elseif (is_object($source) && isset($source->{$fieldName})) {
      $property = $source->{$fieldName};
    }
    // Allow methods on wrapper objects with the same name to be used as
    // callbacks to resolve the field value.
    elseif (is_callable([$source, $fieldName])) {
      $property = [$source, $fieldName];
    }
    if (is_callable($property)) {
      return $property($source, $args, $context, $info, $fieldContext);
    }
    return $property;
  }

  /**
   * Gives back the content type mapping, also provides the possibility to alter it.
   *
   * @return array
   *   The complete content type mapping.
   */
  protected function getContentTypeBundleMapping(): array {
    $mapping = [
      'content_page' => 'ContentPage',
    ];

    $mappingEvent = new ContentTypeBundleMappingEvent($mapping);
    $this->eventDispatcher->dispatch($mappingEvent, PepperGraphqlEvents::CONTENT_TYPE_BUNDLE_MAPPING);

    return $mappingEvent->getMapping();
  }

}
