<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Buffers\EntityBuffer;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\path_alias\AliasManager;
use Drupal\pepper_graphql\Wrapper\Routing\EntityResponse;
use Drupal\pepper_graphql\Wrapper\Routing\ErrorResponse;
use Drupal\pepper_graphql\Wrapper\Routing\RedirectResponse;
use Drupal\redirect\RedirectRepository;
use GraphQL\Deferred;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Data producer for route loads.
 *
 * @DataProducer(
 *   id = "pepper_route_load",
 *   name = @Translation("Load route"),
 *   description = @Translation("Loads a route."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Response")
 *   ),
 *   consumes = {
 *     "path" = @ContextDefinition("string",
 *       label = @Translation("Path")
 *     ),
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("Language id"),
 *       default_value = ""
 *     )
 *   }
 * )
 */
class PepperRouteLoad extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The path validator service.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Redirect repository.
   *
   * @var \Drupal\redirect\RedirectRepository|null
   */
  protected $redirectRepository;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupal config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Graphql enity buffer.
   *
   * @var \Drupal\graphql\GraphQL\Buffers\EntityBuffer
   */
  protected $entityBuffer;

  /**
   * Path alias manager.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected $aliasManager;


  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.validator'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('graphql.buffer.entity'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('redirect.repository', ContainerInterface::NULL_ON_INVALID_REFERENCE),
      $container->get('path_alias.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * RouteLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Path\PathValidatorInterface $pathValidator
   *   The path validator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Drupal\graphql\GraphQL\Buffers\EntityBuffer $entityBuffer
   *   The graphql entity buffer.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Drupal logger channel.
   * @param \Drupal\redirect\RedirectRepository|null $redirectRepository
   *   The redirect repository service if the module is enabled.
   * @param \Drupal\path_alias\AliasManager $aliasManager
   *   The path alias manager service.
   */
  public function __construct(
    array                         $configuration,
                                  $pluginId,
                                  $pluginDefinition,
    PathValidatorInterface        $pathValidator,
    EntityTypeManagerInterface    $entityTypeManager,
    LanguageManagerInterface      $languageManager,
    EntityBuffer                  $entityBuffer,
    ConfigFactoryInterface        $configFactory,
    LoggerChannelFactoryInterface $loggerChannelFactory,
    RedirectRepository            $redirectRepository = NULL,
    AliasManager                  $aliasManager,
    ModuleHandlerInterface        $module_handler
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->pathValidator = $pathValidator;
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
    $this->entityBuffer = $entityBuffer;
    $this->configFactory = $configFactory;
    $this->redirectRepository = $redirectRepository;
    $this->logger = $loggerChannelFactory;
    $this->aliasManager = $aliasManager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Resolve method.
   *
   * @param string $path
   *   Url path the resolve.
   * @param string $language
   *   The language id to get the content for.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   Field context.
   *
   * @return \GraphQL\Deferred|\Drupal\pepper_graphql\Wrapper\Routing\RouteResponseInterface
   *   A promise or a response.
   *
   * @throws \GraphQL\Error\Error
   */
  public function resolve($path, $language, FieldContext $context) {

    if (!$this->checkLeadingSlash($path)) {
      return new ErrorResponse(404, 'A path argument has to start with one single slash');
    }

    // The language parameter is optional, if not set - use default.
    if (empty($language)) {
      $language = $this->languageManager->getDefaultLanguage()->getId();
    }

    $context->setContextLanguage($language);
    $context->setContextValue('language', $language);

    // Check if there's an entry in the redirect repository.
    if ($this->redirectRepository) {
      if ($redirect = $this->redirectRepository->findMatchingRedirect($path, [], $language)) {
        if ($redirect instanceof CacheableDependencyInterface) {
          $context->addCacheableDependency($redirect);
        }
        return new RedirectResponse($redirect->getRedirectUrl()->toString(TRUE)->getGeneratedUrl(), (int) $redirect->getStatusCode());
      }
    }

    // The incoming path has no language prefix, so we append it here by looking
    // up the language path prefix and prepending it to the path.

    if (!$url = $this->resolveUrl($path, $context)) {
      $this->logPageNotFound($path, $language);
      return new ErrorResponse(404, 'Route not found');
    }

    $url->setOption('language', $this->languageManager->getLanguage($language));

    $generated = $url->toString(TRUE);
    $context->addCacheableDependency($generated);
    $target = $generated->getGeneratedUrl();

    // If the $path is the front page a redirect is forced but not needed.
    // Todo: Skip this for the POC.
    // if ($target !== $this->getPathWithLangcode($path, $language) && $path != '/') {
    //  return new RedirectResponse($target);
    // }
    [$kind, $type] = explode('.', $url->getRouteName());
    if ($kind !== 'entity') {
      $this->logPageNotFound($path, $language);
      return new ErrorResponse(404);
    }

    $parameters = $url->getRouteParameters();
    $id = $parameters[$type];

    return $this->loadContent($type, $id, $context, $language, $path);
  }

  /**
   * Checks if a path starts with one single leading slash.
   *
   * @param string $path
   *   Path argument.
   *
   * @return bool
   *   True if the slash is correct, false otherwise.
   */
  protected function checkLeadingSlash($path) {
    return '/' . ltrim($path, '/') === $path;
  }

  /**
   * Returns the path prefix for a language id.
   *
   * @param string $langcode
   *   Drupal langcode.
   *
   * @return string
   *   The url prefix to use for that language.
   *
   * @throws \GraphQL\Error\Error
   */
  protected function getPathPrefix($langcode) {
    $mapping = $this->configFactory->get('language.negotiation')->get('url.prefixes');

    if ($mapping === NULL) {
      // There's no language configured.
      return '';
    }


    if (isset($mapping[$langcode])) {
      return $mapping[$langcode];
    }

    throw new Error('Path prefix cannot be found in configuration.');
  }

  /**
   * Resolve an url by path.
   *
   * @param string $path
   *   Incoming path argument.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return \Drupal\Core\Url|null
   *   Url object
   */
  protected function resolveUrl($path, RefinableCacheableDependencyInterface $metadata) {
    if ($this->redirectRepository) {
      if ($redirect = $this->redirectRepository->findMatchingRedirect($path, [])) {
        return $redirect->getRedirectUrl();
      }
    }
    if (($url = $this->pathValidator->getUrlIfValid($path)) && $url->isRouted() && $url->access()) {
      return $url;
    }
    $metadata->addCacheTags(['4xx-response']);
    return NULL;
  }

  /**
   * Loads the entity by type and id.
   *
   * @param string $type
   *   The entity type to load.
   * @param string $id
   *   The id of the entity type to load.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   Field context.
   * @param string $language
   *   The langcode to load the content for.
   * @param string $path_with_langcode
   *   The "resulting" path - just for log purposes.
   *
   * @return \GraphQL\Deferred
   *   A deferred object loading the entity.
   */
  protected function loadContent($type, $id, FieldContext $context, $language, $path) {

    $resolver = $this->entityBuffer->add($type, $id);

    // From \Drupal\graphql\Plugin\GraphQL\DataProducer\Routing\RouteEntity.
    return new Deferred(function () use ($type, $id, $resolver, $context, $language, $path) {
      if (!$entity = $resolver()) {
        // If there is no entity with this id, add the list cache tags so that
        // the cache entry is purged whenever a new entity of this type is
        // saved.
        $type = $this->entityTypeManager->getDefinition($type);
        /** @var \Drupal\Core\Entity\EntityTypeInterface $type */
        $tags = $type->getListCacheTags();
        $context->addCacheTags($tags)->addCacheTags(['4xx-response']);
        $this->logPageNotFound($path, $language);
        return new ErrorResponse(404);
      }

      // Check Rabbit Hole behaviour for entity.
      if ($this->moduleHandler->moduleExists('rabbit_hole')) {
        $behavoir = \Drupal::service('rabbit_hole.behavior_invoker')->getBehaviorPlugin($entity);
        if (isset($behavoir->pluginId) && ($behavoir->pluginId == 'page_not_found' || $behavoir->pluginId == 'access_denied')) {
          if ($behavoir->pluginId == 'page_not_found') {
            return new ErrorResponse(404);
          }
          if ($behavoir->pluginId == 'access_denied') {
            return new ErrorResponse(403);
          }
        }
      }

      // Get the correct translation.
      if (isset($language) && $language != $entity->language()->getId() && $entity instanceof TranslatableInterface) {
        if (!$entity->hasTranslation($language)) {
          /** @var \Drupal\Core\Entity\EntityTypeInterface $type */
          $type = $this->entityTypeManager->getDefinition($type);
          $tags = $type->getListCacheTags();
          $context->addCacheTags($tags)->addCacheTags(['4xx-response']);
          $this->logPageNotFound($path, $language);
          return new ErrorResponse(404, sprintf('No translation found for %s', $language));
        }

        $entity = $entity->getTranslation($language);
        $entity->addCacheContexts(["static:language:{$language}"]);
      }

      // Check if url is drupal route /node/nid and redirect to url alias.
      if (preg_match('/\/node\/(\d+)/', $path, $matches)) {
        // Extract the ID from the regex match.
        $id = $matches[1];
        if (!empty($id) && is_numeric($id)) {
          // Get url for drupal route.
          $url_alias = $this->aliasManager->getAliasByPath('/node/' . $id, $language);
          // Todo: Should we add $entity as addCacheableDependency?
          $context->addCacheTags(['node:' . $id]);
          if (!empty($url_alias) && $path != $url_alias) {
            return new RedirectResponse("/" . $language . $url_alias, 301);
          }
        }
      }

      $access = $entity->access('view', NULL, TRUE);
      $context->addCacheableDependency($access);
      if ($access->isAllowed()) {
        return new EntityResponse($entity);
      }

      $type = $this->entityTypeManager->getDefinition($type);
      /** @var \Drupal\Core\Entity\EntityTypeInterface $type */
      $tags = $type->getListCacheTags();
      $context->addCacheTags($tags)->addCacheTags(['4xx-response']);

      // Permission denied.
      return new ErrorResponse(403);
    });

  }

  /**
   * Returns the resulting path with language prefix and path.
   *
   * @param string $path
   *   Drupal path without langcode.
   * @param string $language_id
   *   Language ID - not langcode.
   *
   * @return string
   *   Full path.
   */
  protected function getPathWithLangcode($path, $language_id) {
    return sprintf('/%s/%s', $this->getPathPrefix($language_id), ltrim($path, '/'));
  }

  /**
   * Create a page not found log entry.
   *
   * @param string $path
   *   Path that was not found.
   * @param string $language
   *   Path that was not found.
   *
   * @todo This needs a refactoring with clean DI.
   */
  protected function logPageNotFound($path, $language) {

    $this->logger->get('page not found')->warning('@uri', ['@uri' => $this->getPathWithLangcode($path, $language)]);


    if ($this->moduleHandler->moduleExists('redirect_404')) {

      // Ignore paths specified in the redirect settings.
      if ($pages = mb_strtolower($this->configFactory->get('redirect_404.settings')->get('pages'))) {
        // Do not trim a trailing slash if that is the complete path.
        $path_to_match = $path === '/' ? $path : rtrim($path, '/');

        if (\Drupal::service('path.matcher')->matchPath(mb_strtolower($path_to_match), $pages)) {
          return;
        }
      }
      $redirectStorage = \Drupal::service('redirect.not_found_storage');
      // Write record.
      $redirectStorage->logRequest($path, $language);
    }
  }

}
