<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\file\Entity\File;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;
use GraphQL\Deferred;
use GraphQL\Exception\InvalidArgument;

/**
 * @DataProducer(
 *   id = "media_entity_image_responsive",
 *   name = @Translation("Media Entity Image Responsive"),
 *   description = @Translation("Provides sources for responsive images."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "input" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *    "info" = @ContextDefinition("string",
 *       label = @Translation("Information")
 *     ),
 *    "style" = @ContextDefinition("string",
 *       label = @Translation("Responsive Image Style")
 *     )
 *   }
 * )
 */
class MediaEntityImageResponsive extends DataProducerPluginBase {

  /**
   * Returns specific image infos.
   *
   * @param EntityInterface $input
   *   The input array.
   * @param string $info
   *   The info which is seeked.
   * @param string style
   *   Id of the responsive image style (used for sources only).
   *
   * @return \GraphQL\Deferred
   *   The found information.
   */
  public function resolve(EntityInterface $input, string $info, string $style) {
    $value = '';
    $values = $input->get('field_media_image')->first()->getValue();
    $file = File::load($values['target_id']);

    switch ($info) {
      case 'sources':
        $value = [];
        $variables = [
          'uri' => $file->getFileUri(),
          'width' => $values['width'],
          'height' => $values['height'],
        ];
        $sources = [];
        $responsive_image_style = ResponsiveImageStyle::load($style);

        if (empty($responsive_image_style)) {
          throw new InvalidArgument(sprintf('Unknown responsive image style: %s', $style));
        }

        $breakpoints = array_reverse(\Drupal::service('breakpoint.manager')->getBreakpointsByGroup($responsive_image_style->getBreakpointGroup()));
        foreach ($responsive_image_style->getKeyedImageStyleMappings() as $breakpoint_id => $multipliers) {
          if (isset($breakpoints[$breakpoint_id])) {
            $sources[] = _pepper_graphql_responsive_image_build_source_attributes($variables, $breakpoints[$breakpoint_id], $multipliers);
          }
        }
        foreach ($sources as $source) {
          $value[] = [
            'style' => $responsive_image_style,
            'source' => $source,
          ];
        }
        break;
    }
    return new Deferred(function () use ($value) {
      return $value;
    });
  }

}
