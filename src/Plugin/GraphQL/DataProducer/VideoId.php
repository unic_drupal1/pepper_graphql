<?php


namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Gets the id of the video for youtube or vimeo videos.
 *
 * @DataProducer(
 *   id = "video_id",
 *   name = @Translation("Video id"),
 *   description = @Translation("Returns the id of the remote video extracted from the url."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Video id")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity")
 *     )
 *   }
 * )
 */
class VideoId extends DataProducerPluginBase {


  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return string
   */
  public function resolve(EntityInterface $entity) {
    if ($entity->hasField('field_media_oembed_video')) {
      $url = $entity->get('field_media_oembed_video')->value;

      if (strpos($url, 'vimeo') !== FALSE) {
        return $this->extractVimeoId($url);
      }
      else {
        return $this->extractYoutubeId($url);
      }
    }
  }

  /**
   * Extracts the youtube id from a youtube url.
   *
   * @param string $url
   *   The url to extract the id from.
   *
   * @return string
   *   The extracted youtube id.
   *
   * @see https://stackoverflow.com/questions/3392993/php-regex-to-get-youtube-video-id
   */
  private function extractYoutubeId($url) {
    if (preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches)) {
      return $matches[0];
    }
  }

  /**
   * Extracts the vimeo id from a vimeo url.
   *
   * @param string $url
   *   The url to extract the id from.
   *
   * @return string
   *   The extracted vimeo id.
   *
   * @param $string
   *
   * @see https://stackoverflow.com/questions/10488943/easy-way-to-get-vimeo-id-from-a-vimeo-url
   */
  private function extractVimeoId($url) {
    if (preg_match('#(https?://)?(www.)?(player.)?vimeo.com/([a-z]*/)*([0-9]{6,11})[?]?.*#', $url, $matches)) {
      return $matches[5];
    }
  }

}
