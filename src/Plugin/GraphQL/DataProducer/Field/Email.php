<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\Field;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "pepper_graphql_email",
 *   name = @Translation("Email"),
 *   description = @Translation("Returns a value from an email field."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity")
 *     ),
 *     "field" = @ContextDefinition("any",
 *       label = @Translation("Field")
 *     ),
 *   }
 * )
 */
class Email extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * PepperRouteItems constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * @param $entity
   *   The node.
   * @param string $field
   *   The field that holds the value.
   * @param FieldContext $context
   *   The field context.
   *
   * @return String
   */
  public function resolve($entity, string $field, FieldContext $context) {
    return $entity->{$field}->value;
  }
}
