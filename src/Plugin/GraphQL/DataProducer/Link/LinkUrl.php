<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\Link;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Url;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\token\MenuLinkFieldItemList;

/**
 * Returns the url of a link.
 *
 * @DataProducer(
 *   id = "pepper_link_url",
 *   name = @Translation("Pepper Link UrL"),
 *   description = @Translation("Returns the url of a link."),
 *   produces = @ContextDefinition("String",
 *     label = @Translation("Url")
 *   ),
 *   consumes = {
 *     "link" = @ContextDefinition("any",
 *       label = @Translation("Link")
 *     ),
 *    "language" = @ContextDefinition("string",
 *       label = @Translation("Context language")
 *     ),
 *   }
 * )
 */
class LinkUrl extends DataProducerPluginBase {

  /**
   * @param $link
   * @param $language
   * @return string|null
   */
  public function resolve($link, $language) {
    if ($link instanceof Link) {
      return $link->getUrl()->toString(TRUE)->getGeneratedUrl();
    }
    elseif (isset($link['uri'])) {
      return Url::fromUri($link['uri'])->toString(TRUE)->getGeneratedUrl();
    }
    else {
      return NULL;
    }
  }

}
