<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\Link;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Url;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\token\MenuLinkFieldItemList;

/**
 * Returns the title of a link.
 *
 * @DataProducer(
 *   id = "pepper_link_title",
 *   name = @Translation("Pepper Link Title"),
 *   description = @Translation("Returns the title of a link."),
 *   produces = @ContextDefinition("String",
 *     label = @Translation("Title")
 *   ),
 *   consumes = {
 *     "link" = @ContextDefinition("any",
 *       label = @Translation("Link")
 *     ),
 *    "language" = @ContextDefinition("string",
 *       label = @Translation("Context language")
 *     ),
 *   }
 * )
 */
class LinkTitle extends DataProducerPluginBase {

  /**
   * @param $link
   * @param $language
   * @return array|\Drupal\Component\Render\MarkupInterface|string|null
   */
  public function resolve($link, $language) {
    if ($link instanceof Link) {
      return $link->getText();
    }
    elseif (isset($link['title'])) {
      return $link['title'];
    }
    else {
      return NULL;
    }
  }

}
