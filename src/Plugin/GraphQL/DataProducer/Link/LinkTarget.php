<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\Link;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Url;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\token\MenuLinkFieldItemList;

/**
 * Returns the target attribute of a link.
 *
 * @DataProducer(
 *   id = "pepper_link_target",
 *   name = @Translation("Pepper Link Target"),
 *   description = @Translation("Returns the target attribute of a link."),
 *   produces = @ContextDefinition("String",
 *     label = @Translation("Target")
 *   ),
 *   consumes = {
 *     "link" = @ContextDefinition("any",
 *       label = @Translation("Link")
 *     ),
 *    "language" = @ContextDefinition("string",
 *       label = @Translation("Context language")
 *     ),
 *   }
 * )
 */
class LinkTarget extends DataProducerPluginBase {

  /**
   * @param $link
   * @param $language
   * @return array|\Drupal\Component\Render\MarkupInterface|string|null
   */
  public function resolve($link, $language) {
    if ($link instanceof Link) {
      $attributes = $link->getUrl()->getOption('attributes');
      return isset($attributes['target']) ? $attributes['target'] : NULL;
    }
    elseif (isset($link['options']['attributes']['target'])) {
      return $link['options']['attributes']['target'];
    }
    else {
      return NULL;
    }
  }

}
