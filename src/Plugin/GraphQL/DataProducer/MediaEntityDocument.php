<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\file\Entity\File;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use GraphQL\Deferred;

/**
 * @DataProducer(
 *   id = "media_entity_document",
 *   name = @Translation("Media Entity Document"),
 *   description = @Translation("Provides Infos about Document medias."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "input" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *    "info" = @ContextDefinition("string",
 *       label = @Translation("Information")
 *     )
 *   }
 * )
 */
class MediaEntityDocument extends DataProducerPluginBase {

  /**
   * Returns specific image infos.
   *
   * @param EntityInterface $input
   *   The input media entity.
   * @param string $info
   *   The info which is seeked.
   *
   * @return \GraphQL\Deferred
   *   The found information.
   */
  public function resolve(EntityInterface $input, string $info) {
    $value = '';
    $values = $input->get('field_media_document')->first()->getValue();
    $file = File::load($values['target_id']);

    switch ($info) {
      case 'id':
        $value = $values['target_id'];
        break;

      case 'src':
        $uri = $file->getFileUri();
        $value = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
        break;

      case 'mimetype':
        $value = $file->getMimeType();
        break;

      case 'filesize':
        $value = $file->getSize();
        break;

      case 'extension':
        $value = pathinfo($file->getFileUri())['extension'];
        break;
    }
    return new Deferred(function () use ($value) {
      return $value;
    });
  }

}
