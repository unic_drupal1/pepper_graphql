<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\file\Entity\File;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\image\Entity\ImageStyle;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

/**
 * @DataProducer(
 *   id = "media_entity_image_responsive_fallback",
 *   name = @Translation("Media Entity Image Responsive Fallback"),
 *   description = @Translation("Provides a fallback image for a responsive image style."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "input" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *    "style" = @ContextDefinition("string",
 *       label = @Translation("Responsive Image Style")
 *     )
 *   }
 * )
 */
class MediaEntityImageResponsiveFallback extends DataProducerPluginBase {

  /**
   * Returns specific image infos.
   *
   * @param EntityInterface $input
   *   The input array.
   * @param string $style
   *   Id of the responsive image style (used for sources only).
   *
   * @return mixed
   *   The fallback image.
   */
  public function resolve(EntityInterface $input, $style) {
    $values = $input->get('field_media_image')->first()->getValue();
    $file = File::load($values['target_id']);
    $image_uri = $file->getFileUri();

    $responsive_image_style = ResponsiveImageStyle::load($style);
    $fallback_style = ImageStyle::load($responsive_image_style->getFallbackImageStyle());
    $fallback = $fallback_style->buildUrl($image_uri);
    return $fallback;
  }

}
