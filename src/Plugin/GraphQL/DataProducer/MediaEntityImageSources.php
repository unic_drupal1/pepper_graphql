<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use GraphQL\Deferred;

/**
 * @DataProducer(
 *   id = "media_entity_image_sources",
 *   name = @Translation("Media Entity Image Sources"),
 *   description = @Translation("Uses responsive image style to create different image sources."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "input" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *    "info" = @ContextDefinition("string",
 *       label = @Translation("Information")
 *     )
 *   }
 * )
 */
class MediaEntityImageSources extends DataProducerPluginBase {

  /**
   * Maps the requested info ids to the provided ids.
   *
   * @var array
   */
  protected $infoMapping = [
    'src' => 'srcset',
    'media' => 'media',
    'sizes' => 'sizes',
  ];

  /**
   * @param array $input
   *   The input array.
   * @param string $info
   *   The info which is seeked.
   * @return mixed
   *   The element at the specified position.
   */
  public function resolve(array $input, $info) {
    $storage = $input['source']->storage();
    $infoMapping = $this->infoMapping;
    return new Deferred(function () use ($storage, $infoMapping, $info) {
      // Sometimes the media value is not set.
      if (isset($storage[$infoMapping[$info]])) {
        return $storage[$infoMapping[$info]]->value();
      }
      return NULL;
    });
  }

}
