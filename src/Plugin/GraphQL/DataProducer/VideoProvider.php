<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Gets the id of the video for youtube or vimeo videos.
 *
 * @DataProducer(
 *   id = "video_provider",
 *   name = @Translation("Video provider"),
 *   description = @Translation("Returns the provider name of the remote video extracted from the url."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Video provider")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity")
 *     )
 *   }
 * )
 */
class VideoProvider extends DataProducerPluginBase {

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return string
   */
  public function resolve(EntityInterface $entity) {
    if ($entity->hasField('field_media_oembed_video')) {
      $url = $entity->get('field_media_oembed_video')->value;

      if (strpos($url, 'vimeo') !== FALSE) {
        return 'vimeo';
      }
      else if (strpos($url, 'youtu') !== FALSE) {
        return 'youtube';
      }
    }
  }
}
