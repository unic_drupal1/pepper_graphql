<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Data producer for route loads.
 *
 * @DataProducer(
 *   id = "pepper_entity_translations",
 *   name = @Translation("Loads all node items that are active"),
 *   description = @Translation("tbd"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Response")
 *   ),
 *   consumes = {
 *     "entities" = @ContextDefinition("any",
 *       label = @Translation("Entities"),
 *       multiple = true
 *     )
 *   }
 * )
 */
class PepperEntityTranslations extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * PepperRouteItems constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param $entityTypeManager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }


  public function resolve($entities) {

    $return = [];

    /**
     * @var \Drupal\Core\Entity\TranslatableInterface $entity
     */
    foreach ($entities as $key => $entity) {
      $return[] = $entity;
      if ($entity instanceof TranslatableInterface) {
        foreach ($entity->getTranslationLanguages(FALSE) as $translationLanguage) {
          $return[] = $entity->getTranslation($translationLanguage->getId());
        }
      }
    }
    return $return;

  }

}
