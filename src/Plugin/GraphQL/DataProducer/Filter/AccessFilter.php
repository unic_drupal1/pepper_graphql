<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\Filter;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "access_filter",
 *   name = @Translation("Access Filter"),
 *   description = @Translation("Filters out entities that are not accessible"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "entities" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *   }
 * )
 */
class AccessFilter extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The context language for this request.
   *
   * @var string
   */
  protected $language;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   */
  public function __construct(
    array $configuration,
    string $pluginId,
    array $pluginDefinition
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Filters out entities, that have no translations for the current language.
   *
   * @param array $entities
   *   Given entities.
   *
   * @return mixed
   *   Entities with a translation for the current language.
   */
  public function resolve(array $entities) {
    return array_filter($entities, function($item) {
      return $item->access instanceof AccessResultInterface && $item->access->isAllowed();
    });
  }

}
