<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\Filter;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "language_filter",
 *   name = @Translation("Language Filter"),
 *   description = @Translation("Filters out entities with no translation for the current language"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "entities" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *   }
 * )
 */
class LanguageFilter extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The menu link content storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $menuLinkContentStorage;

  /**
   * The context language for this request.
   *
   * @var string
   */
  protected $language;

  /**
   * The entities, that are not filtered out.
   *
   * @var array
   */
  protected $keptEntities = [];

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    array $configuration,
    string $pluginId,
    array $pluginDefinition,
    EntityTypeManager $entityTypeManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->menuLinkContentStorage = $this->entityTypeManager->getStorage('menu_link_content');
  }

  /**
   * Filters out entities, that have no translations for the current language.
   *
   * @param array $entities
   *   Given entities.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   The Context for this request.
   *
   * @return mixed
   *   Entities with a translation for the current language.
   */
  public function resolve(array $entities, FieldContext $context) {
    $this->language = $context->getContextLanguage();
    foreach ($entities as $entity) {
      switch (get_class($entity)) {
        case 'Drupal\Core\Menu\MenuLinkTreeElement':
          $this->handleMenuItem($entity);
          break;

        default:
          // The entity type is not handled yet, so simply give it back.
          $this->keptEntities[] = $entity;
      }
    }
    return $this->keptEntities;
  }

  /**
   * Checks if a translation is available for this MenuLinkTreeElement.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement $entity
   *   The given MenuLinkTreeElement.
   */
  private function handleMenuItem(MenuLinkTreeElement $entity) {
    /** @var  \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $link */
    $link = $entity->link;
    $plugin_definition = $link->getPluginDefinition();
    if (isset($plugin_definition['metadata']['entity_id'])) {
      $id = $plugin_definition['metadata']['entity_id'];
      $linkEntity = $this->menuLinkContentStorage->load($id);

      // Check if referenced node in menu item has a translation for current language.
      $hasTranslation = TRUE;
      if (!empty($linkEntity->get('link')->getValue()) && $link = Url::fromUri($linkEntity->get('link')->getValue()[0]['uri'])) {
        if (!$link->isExternal() && $link->isRouted() && isset($link->getRouteParameters()['node'])) {
          if ($node = Node::load($link->getRouteParameters()['node'])) {
            if (!$node->hasTranslation($this->language)) {
              $hasTranslation = FALSE;
            }
          }
        }
      }

      // If link entity and referenced node has translation add it to menu.
      if ($linkEntity->hasTranslation($this->language) && $hasTranslation) {
        $this->keptEntities[] = $entity;
      }
    }
    elseif ($plugin_definition['route_name'] == "<front>") {
      $this->keptEntities[] = $entity;
    }

  }

}
