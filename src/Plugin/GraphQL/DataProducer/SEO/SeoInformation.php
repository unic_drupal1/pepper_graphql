<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\SEO;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\Renderer;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use GraphQL\Deferred;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "seo_information",
 *   name = @Translation("Seo Information"),
 *   description = @Translation("Provides seo information."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "input" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *    "info" = @ContextDefinition("string",
 *       label = @Translation("Information")
 *     )
 *   }
 * )
 */
class SeoInformation extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Maps the metatag information structure.
   *
   * @var array
   */
  protected $seoStructure = [
    'meta_description' => [
      'key' => 'description',
      'attribute' => 'content',
      'label' => 'name',
    ],
    'title' => [
      'key' => 'title',
      'attribute' => 'content',
      'label' => 'name',
    ],
    'canonical' => [
      'key' => 'canonical',
      'attribute' => 'href',
      'label' => 'rel',
    ],
    'robots' => [
      'key' => 'robots',
      'attribute' => 'content',
      'label' => 'name',
    ],
    'keywords' => [
      'key' => 'keywords',
      'attribute' => 'content',
      'label' => 'name',
    ],
  ];

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer')
    );
  }

  /**
   * SeoInformation constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   *
   * @codeCoverageIgnore
   */
  public function __construct(array $configuration, $pluginId, array $pluginDefinition, Renderer $renderer) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->renderer = $renderer;
  }

  /**
   * Returns specific seo information.
   *
   * @param $input
   *   The input array.
   * @param string $info
   *   The info which is seeked.
   *
   * @return \GraphQL\Deferred
   *   The found information.
   */
  public function resolve($input, string $info, FieldContext $context) {
    $value = '';

    switch ($info) {
      case 'metatags':
        $value = $input;
        break;

      case 'meta_description':
        $value = $this->getTagByKey($input, 'meta_description');
        break;

      case 'page_title':
        $value = $this->getTagByKey($input, 'title');
        break;

      case 'canonical':
        $url = $input->toUrl()->toString(TRUE);
        $value = $url->getGeneratedUrl();
        break;

      case 'meta_name_robots':
        $value = $this->getTagByKey($input, 'robots');
        break;

      case 'keywords':
        $value = $this->getTagByKey($input, 'keywords');
        break;

      case 'hreflang':
        $language = $context->getContextLanguage();
        $value = $language;
    }
    return new Deferred(function () use ($value) {
      return $value;
    });
  }

  /**
   * Gives back the specific metatag-content of an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity which meta tags are needed.
   * @param string $key
   *   The tag key.
   *
   * @return string
   *   The resolved token, or the custom content.
   */
  protected function getTagByKey(EntityInterface $entity, $key) {
    $context = new RenderContext();
    $result = $this->renderer->executeInRenderContext($context, function () use ($entity, $key) {
      $tags = metatag_get_tags_from_route($entity);
      // Return NULL if metatags are missing, which can happen in preview
      // if a node is not saved yet.
      if (empty($tags) || !isset($tags["#attached"]["html_head"])) {
        return NULL;
      }
      foreach ($tags["#attached"]["html_head"] as $tag) {
        if (isset($tag[0]['#attributes'][$this->seoStructure[$key]['label']])
          && $tag[0]['#attributes'][$this->seoStructure[$key]['label']] == $this->seoStructure[$key]['key']) {
          return $tag[0]['#attributes'][$this->seoStructure[$key]['attribute']];
        }
      }
      return NULL;
    });
    if (!$context->isEmpty()) {
      $bubbleable_metadata = $context->pop();
      BubbleableMetadata::createFromObject($result)->merge($bubbleable_metadata);
    }
    if ($result) {
      return $result;
    }
    return '';
  }

}
