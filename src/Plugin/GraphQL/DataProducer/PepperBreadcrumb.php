<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Url;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\token\MenuLinkFieldItemList;

/**
 * Returns an array of links that define the breadcrumb for the entity.
 *
 * @DataProducer(
 *   id = "pepper_breadcrumb",
 *   name = @Translation("Pepper Breadcrumb"),
 *   description = @Translation("Returns breadcrumb links for the entity."),
 *   produces = @ContextDefinition("Link",
 *     label = @Translation("Links")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity")
 *     ),
 *    "language" = @ContextDefinition("string",
 *       label = @Translation("Context language")
 *     ),
 *   }
 * )
 */
class PepperBreadcrumb extends DataProducerPluginBase {

  /**
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $language
   * @param string $field_paragraph
   * @param string $field_node
   */
  public function resolve(EntityInterface $entity, $language) {
    if (!($entity instanceof NodeInterface)) {
      return NULL;
    }
    // Load all menu links for a node.
    $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
    $menu_links = $menu_link_manager->loadLinksByRoute('entity.node.canonical', array('node' => $entity->id()));

    if (!empty($menu_links)) {
      $links = [];
      /** @var  $menu_link MenuLinkInterface */
      $menu_link = reset($menu_links);
      $title = $menu_link->getTitle();
      $routeName = $menu_link->getRouteName();
      $routeParams = $menu_link->getRouteParameters();
      $link = Link::createFromRoute($title, $routeName, $routeParams);
      $links[] = $link;

      while ($parent = $menu_link->getParent()) {
        $parentLink = $menu_link_manager->createInstance($parent);
        $title = $parentLink->getTitle();
        $routeName = $parentLink->getRouteName();
        $routeParams = $parentLink->getRouteParameters();
        $link = Link::createFromRoute($title, $routeName, $routeParams);
        $links[] = $link;
        $menu_link = $parentLink;
      }

      if (!empty($links)) {
        // Make the breadcrumb items appear in ascending order.
        $links = array_reverse($links);
        // Check if the first link is the link to the frontpage or not.
        /** @var Link $firstLink */
        $firstLink = reset($links);
        if ($firstLink->getUrl()->toString(TRUE) !== Url::fromRoute('<front>')->toString(TRUE)) {
          $homeLink = Link::createFromRoute('Home', '<front>');
          array_unshift($links, $homeLink);
        }

        return $links;
      }
    }
    return NULL;
  }

}
