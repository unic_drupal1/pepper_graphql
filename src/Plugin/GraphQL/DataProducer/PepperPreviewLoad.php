<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\graphql\GraphQL\Buffers\EntityBuffer;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\pepper_graphql\Wrapper\Routing\EntityResponse;
use Drupal\pepper_graphql\Wrapper\Routing\ErrorResponse;
use Drupal\pepper_graphql\Wrapper\Routing\RedirectResponse;
use Drupal\redirect\RedirectRepository;
use GraphQL\Deferred;
use GraphQL\Error\Error;
use GraphQL\Server\OperationParams;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Data producer for route loads.
 *
 * @DataProducer(
 *   id = "pepper_preview_load",
 *   name = @Translation("Load route"),
 *   description = @Translation("Loads an entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Response")
 *   ),
 *   consumes = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("uuid")
 *     ),
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("Language id"),
 *       default_value = ""
 *     )
 *   }
 * )
 */
class PepperPreviewLoad extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The path validator service.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Redirect repository.
   *
   * @var \Drupal\redirect\RedirectRepository|null
   */
  protected $redirectRepository;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * Drupal config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * Graphql enity buffer.
   *
   * @var \Drupal\graphql\GraphQL\Buffers\EntityBuffer
   */
  private $entityBuffer;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tempstore.shared'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('graphql.buffer.entity'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('redirect.repository', ContainerInterface::NULL_ON_INVALID_REFERENCE)
    );
  }

  /**
   * RouteLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Path\PathValidatorInterface $pathValidator
   *   The path validator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Drupal\graphql\GraphQL\Buffers\EntityBuffer $entityBuffer
   *   The graphql entity buffer.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Drupal logger channel.
   * @param \Drupal\redirect\RedirectRepository|null $redirectRepository
   *   The redirect repository service if the module is enabled.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    SharedTempStoreFactory $tempstoreFactory,
    EntityTypeManagerInterface $entityTypeManager,
    LanguageManagerInterface $languageManager,
    EntityBuffer $entityBuffer,
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactoryInterface $loggerChannelFactory,
    RedirectRepository $redirectRepository = NULL
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->tempstoreFactory = $tempstoreFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
    $this->entityBuffer = $entityBuffer;
    $this->configFactory = $configFactory;
    $this->redirectRepository = $redirectRepository;
    $this->logger = $loggerChannelFactory;
  }

  /**
   * Resolve method.
   *
   * @param string $uuid
   *   Uuid to preview.
   * @param string $language
   *   The language id to get the content for.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   Field context.
   *
   * @return \GraphQL\Deferred|\Drupal\pepper_graphql\Wrapper\Routing\RouteResponseInterface
   *   A promise or a response.
   *
   * @throws \GraphQL\Error\Error
   */
  public function resolve($uuid, $language, FieldContext $context) {

    // If we use cookie domain to protect die preview functionality we need
    // to check if the user has permission to access the preview functionality.
    $cookie_domain_enabled = Settings::get('pepper_preview.cookie_domain_override');
    if (!empty($cookie_domain_enabled)) {
      // Check if User has permission to view unpublished content.
      if (!\Drupal::currentUser()->hasPermission('pepper preview functionality')) {
        // Permission denied.
        $login_url = \Drupal::request()->getSchemeAndHttpHost() . "/user/login";
        return new ErrorResponse(
          403,
          'Permission denied. To access the preview functionality, please log in to Drupal first: <a href="' . $login_url . '" target="_blank">' . $login_url . '</a>'
        );
      }
    }

    // Check if there is serialized entity inside.
    $entity = NULL;

    $context->setContextValue('preview', TRUE);

    // The language parameter is optional, if not set - use default.
    if (empty($language)) {
      $language = $this->languageManager->getDefaultLanguage()->getId();
    }

    $context->setContextLanguage($language);
    $context->setContextValue('language', $language);

    if ($context->getFieldName() == 'preview') {
      $tempstore = $this->tempstoreFactory->get('decoupled_preview');
      // Check if we have a serialized object in here.
      if ($serialized = $tempstore->get($uuid)) {
        $entity = unserialize($serialized);
      }
      $context->mergeCacheMaxAge(0);
    }
    return new EntityResponse($entity);
  }

  /**
   * Checks if a path starts with one single leading slash.
   *
   * @param string $path
   *   Path argument.
   *
   * @return bool
   *   True if the slash is correct, false otherwise.
   */
  protected function checkLeadingSlash($path) {
    return '/' . ltrim($path, '/') === $path;
  }

  /**
   * Returns the path prefix for a language id.
   *
   * @param string $langcode
   *   Drupal langcode.
   *
   * @return string
   *   The url prefix to use for that language.
   *
   * @throws \GraphQL\Error\Error
   */
  protected function getPathPrefix($langcode) {
    $mapping = $this->configFactory->get('language.negotiation')->get('url.prefixes');

    if ($mapping === NULL) {
      // There's no language configured.
      return '';
    }


    if (isset($mapping[$langcode])) {
      return $mapping[$langcode];
    }

    throw new Error('Path prefix cannot be found in configuration.');
  }

  /**
   * Resolve an url by path.
   *
   * @param string $path
   *   Incoming path argument.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return \Drupal\Core\Url|null
   *   Url object
   */
  protected function resolveUrl($path, RefinableCacheableDependencyInterface $metadata) {
    if ($this->redirectRepository) {
      if ($redirect = $this->redirectRepository->findMatchingRedirect($path, [])) {
        return $redirect->getRedirectUrl();
      }
    }
    if (($url = $this->pathValidator->getUrlIfValid($path)) && $url->isRouted() && $url->access()) {
      return $url;
    }
    $metadata->addCacheTags(['4xx-response']);
    return NULL;
  }

  /**
   * Loads the entity by type and id.
   *
   * @param string $type
   *   The entity type to load.
   * @param string $id
   *   The id of the entity type to load.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   Field context.
   * @param string $language
   *   The langcode to load the content for.
   * @param string $path_with_langcode
   *   The "resulting" path - just for log purposes.
   *
   * @return \GraphQL\Deferred
   *   A deferred object loading the entity.
   */
  protected function loadContent($type, $id, FieldContext $context, $language, $path) {

    $resolver = $this->entityBuffer->add($type, $id);

    // From \Drupal\graphql\Plugin\GraphQL\DataProducer\Routing\RouteEntity.
    return new Deferred(function () use ($type, $id, $resolver, $context, $language, $path) {
      if (!$entity = $resolver()) {
        // If there is no entity with this id, add the list cache tags so that
        // the cache entry is purged whenever a new entity of this type is
        // saved.
        $type = $this->entityTypeManager->getDefinition($type);
        /** @var \Drupal\Core\Entity\EntityTypeInterface $type */
        $tags = $type->getListCacheTags();
        $context->addCacheTags($tags)->addCacheTags(['4xx-response']);
        $this->logPageNotFound($path, $language);
        return new ErrorResponse(404);
      }

      // Get the correct translation.
      if (isset($language) && $language != $entity->language()->getId() && $entity instanceof TranslatableInterface) {
        if (!$entity->hasTranslation($language)) {
          /** @var \Drupal\Core\Entity\EntityTypeInterface $type */
          $type = $this->entityTypeManager->getDefinition($type);
          $tags = $type->getListCacheTags();
          $context->addCacheTags($tags)->addCacheTags(['4xx-response']);
          $this->logPageNotFound($path, $language);
          return new ErrorResponse(404, sprintf('No translation found for %s', $language));
        }

        $entity = $entity->getTranslation($language);
        $entity->addCacheContexts(["static:language:{$language}"]);
      }

      $access = $entity->access('view', NULL, TRUE);
      $context->addCacheableDependency($access);
      if ($access->isAllowed()) {
        return new EntityResponse($entity);
      }

      $type = $this->entityTypeManager->getDefinition($type);
      /** @var \Drupal\Core\Entity\EntityTypeInterface $type */
      $tags = $type->getListCacheTags();
      $context->addCacheTags($tags)->addCacheTags(['4xx-response']);

      // Permission denied.
      return new ErrorResponse(403);
    });

  }

  /**
   * Returns the resulting path with language prefix and path.
   *
   * @param string $path
   *   Drupal path without langcode.
   * @param string $language_id
   *   Language ID - not langcode.
   *
   * @return string
   *   Full path.
   */
  private function getPathWithLangcode($path, $language_id) {
    return sprintf('/%s/%s', $this->getPathPrefix($language_id), ltrim($path, '/'));
  }

  /**
   * Create a page not found log entry.
   *
   * @param string $path
   *   Path that was not found.
   * @param string $language
   *   Path that was not found.
   *
   * @todo This needs a refactoring with clean DI.
   */
  private function logPageNotFound($path, $language) {

    $this->logger->get('page not found')->warning('@uri', ['@uri' => $this->getPathWithLangcode($path, $language)]);

    $moduleHandler = \Drupal::service('module_handler');

    if ($moduleHandler->moduleExists('redirect_404')) {

      // Ignore paths specified in the redirect settings.
      if ($pages = mb_strtolower($this->configFactory->get('redirect_404.settings')->get('pages'))) {
        // Do not trim a trailing slash if that is the complete path.
        $path_to_match = $path === '/' ? $path : rtrim($path, '/');

        if (\Drupal::service('path.matcher')->matchPath(mb_strtolower($path_to_match), $pages)) {
          return;
        }
      }
      $redirectStorage = \Drupal::service('redirect.not_found_storage');
      // Write record.
      $redirectStorage->logRequest($path, $language);
    }
  }

}
