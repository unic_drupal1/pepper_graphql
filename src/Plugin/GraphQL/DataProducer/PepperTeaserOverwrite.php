<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * @DataProducer(
 *   id = "pepper_graphql_teaser_overwrite",
 *   name = @Translation("Teaser Ovrewrite"),
 *   description = @Translation("Implements teaser overwrite logic."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity")
 *     ),
 *    "overwrite_teaser_field" = @ContextDefinition("string",
 *       label = @Translation("Fieldname Paragraph")
 *     ),
 *    "target_node_field" = @ContextDefinition("string",
 *       label = @Translation("Target Node")
 *     ),
 *   }
 * )
 */
class PepperTeaserOverwrite extends DataProducerPluginBase {

  /**
   * @param Paragraph $paragraph
   * @param $overwrite_teaser_field
   * @param $target_node_field
   * @param $context
   *
   */
  public function resolve($paragraph, $overwrite_teaser_field, $target_node_field, $context) {

    if ($paragraph instanceof Paragraph) {
      $target_node = $this->getTargetNode($paragraph, $target_node_field, $context);
      if ($target_node instanceof Node) {
        switch ($overwrite_teaser_field) {
          case 'title':
            return $this->overwriteTeaserTitle($paragraph, $target_node);

          case 'text':
            return $this->overwriteTeaserText($paragraph, $target_node);

          case 'topline':
            return $this->overwriteTeaserTopline($paragraph, $target_node);

          case 'image':
            return $this->overwriteTeaserImage($paragraph, $target_node);

          default:
            break;
        }

      }
    }
  }

  /**
   * Returns target node entity for teaser paragraph.
   *
   * @param $paragraph
   *   Teaser Paragraph.
   * @param $target_node_field
   *   Field name for target node.
   * @return string
   */
  private function getTargetNode($paragraph, $target_node_field, $context) {
    if ($paragraph->hasField($target_node_field) && !empty($paragraph->get($target_node_field)->getValue())) {
      if ($paragraph->hasField($target_node_field) && !empty($paragraph->get($target_node_field)->getValue())) {
        if (!empty($paragraph->get($target_node_field)->referencedEntities())) {
          $target_node = $paragraph->get($target_node_field)->referencedEntities()['0'];
          if ($target_node->hasTranslation($context->getContextLanguage())) {
            $target_node = $target_node->getTranslation($context->getContextLanguage());
          }
          return $target_node;
        }
      }
    }
  }

  /**
   * Returns paragraph title if set, otherwise teaser title or title from target node will be returned.
   *
   * @param $paragraph
   *   Teaser Paragraph.
   * @param $target_node
   *   Linked node in teaser.
   * @return string
   */
  private function overwriteTeaserTitle($paragraph, $target_node) {
    if ($paragraph->hasField('field_title') && !empty($paragraph->get('field_title')->getValue())) {
      return $paragraph->get('field_title')->value;
    }
    else {
      if ($target_node->hasField('field_teaser_title') && !empty($target_node->get('field_teaser_title')->getValue())) {
        return $target_node->get('field_teaser_title')->value;
      }
      else {
        return $target_node->getTitle();
      }
    }
  }

  /**
   * Returns paragraph text if set, otherwise teaser text or lead from target node will be returned.
   *
   * @param $paragraph
   *   Teaser Paragraph.
   * @param $target_node
   *   Linked node in teaser.
   * @return string
   */
  private function overwriteTeaserText($paragraph, $target_node) {
    if ($paragraph->hasField('field_text') && !empty($paragraph->get('field_text')->getValue())) {
      return $paragraph->get('field_text')->value;
    }
    else {
      if ($target_node->hasField('field_teaser_text') && !empty($target_node->get('field_teaser_text')->getValue())) {
        return $target_node->get('field_teaser_text')->value;
      }
      else {
        if ($target_node->hasField('field_lead') && !empty($target_node->get('field_lead')->getValue())) {
          return $target_node->get('field_lead')->value;
        }
      }
    }
  }


  /**
   * Returns paragraph topline if set, otherwise teaser subtitle from target node will be returned.
   *
   * @param $paragraph
   *   Teaser Paragraph.
   * @param $target_node
   *   Linked node in teaser.
   * @return string
   */
  private function overwriteTeaserTopline($paragraph, $target_node) {
    if ($paragraph->hasField('field_topline') && !empty($paragraph->get('field_topline')->getValue())) {
      return $paragraph->get('field_topline')->value;
    }
    else {
      if ($target_node->hasField('field_teaser_subtitle') && !empty($target_node->get('field_teaser_subtitle')->getValue())) {
        return $target_node->get('field_teaser_subtitle')->value;
      }
    }
  }

  /**
   * Returns paragraph image if set, otherwise teaser image or hero image from target node will be returned.
   *
   * @param $paragraph
   *   Teaser Paragraph.
   * @param $target_node
   *   Linked node in teaser.
   * @return string
   */
  private function overwriteTeaserImage($paragraph, $target_node) {
    if ($paragraph->hasField('field_image') && !empty($paragraph->get('field_image')->getValue())) {
      return $paragraph->get('field_image')->referencedEntities()['0'];
    }
    else {
      // User teaser image from referenced node.
      if ($target_node->hasField('field_teaser_image') && !empty($target_node->get('field_teaser_image')->getValue())) {
        return $target_node->get('field_teaser_image')->referencedEntities()['0'];
      }
      // User hero image from referenced node.
      elseif ($target_node->hasField('field_image') && !empty($target_node->get('field_image')->getValue())) {
        return $target_node->get('field_image')->referencedEntities()['0'];
      }
      // User blog image if referenced node is blog detailpage.
      elseif ($target_node->hasField('field_blog_hero') && !empty($target_node->get('field_blog_hero')->getValue())) {
        return $target_node->get('field_blog_hero')->referencedEntities()['0'];
      }
    }
  }

}
