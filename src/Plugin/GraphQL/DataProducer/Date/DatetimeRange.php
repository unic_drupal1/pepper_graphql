<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\Date;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "pepper_graphql_datetime_range",
 *   name = @Translation("Datetime range"),
 *   description = @Translation("Returns a formatted datetime range."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity")
 *     ),
 *     "field" = @ContextDefinition("any",
 *       label = @Translation("Field")
 *     ),
 *   }
 * )
 */
class DatetimeRange extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * PepperRouteItems constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param $date_formatter
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DateFormatter $date_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('date.formatter')
    );
  }

  /**
   * @param Node $node
   *   The node.
   * @param $field
   *   The field name.
   * @param $context
   *   The context.
   *
   * @return array
   *   Date and time range.
   */
  public function resolve(Node $node, $field, $context) {
    $dateValue = $node->{$field}->start_date;
    $dateValueEnd = $node->{$field}->end_date;
    /**
     * To check the timezone:
     * $dateFormatter->format($dateValue->getTimestamp(), 'custom', 'P');
     */
    $date_range = '';
    $time_range = '';
    if (!empty($dateValue)) {
      $date_range = $this->dateFormatter->format($dateValue->getTimestamp(), 'custom', 'j. M Y', NULL, $context->getContextLanguage());
      $time_range = $this->dateFormatter->format($dateValue->getTimestamp(), 'custom', 'G:i', NULL, $context->getContextLanguage());
    }
    // If there are information about the end of the time range.
    if (!empty($dateValueEnd)) {
      // Create the formatted end values.
      $date_range_end = $this->dateFormatter->format($dateValueEnd->getTimestamp(), 'custom', 'j. M Y', NULL, $context->getContextLanguage());
      $time_range_end = $this->dateFormatter->format($dateValueEnd->getTimestamp(), 'custom', 'G:i', NULL, $context->getContextLanguage());

      // Put start and end together.
      $date_range = $date_range . '–' . $date_range_end;
      $time_range = $time_range . '–' . $time_range_end;

      // Check the year to see if they are the same.
      $startYear = $this->dateFormatter->format($dateValue->getTimestamp(), 'custom', 'Y', NULL, $context->getContextLanguage());
      $endYear = $this->dateFormatter->format($dateValueEnd->getTimestamp(), 'custom', 'Y', NULL, $context->getContextLanguage());
      if ($startYear == $endYear){
        // Only show one year.
        $date_range = $this->dateFormatter->format($dateValue->getTimestamp(), 'custom', 'j. M', NULL, $context->getContextLanguage()) . '–' . $this->dateFormatter->format($dateValueEnd->getTimestamp(), 'custom', 'j. M', NULL, $context->getContextLanguage()) . ' ' . $endYear;

        // Check the month to see if the year and the month they are the same.
        $startMonth = $this->dateFormatter->format($dateValue->getTimestamp(), 'custom', 'M', NULL, $context->getContextLanguage());
        $endMonth = $this->dateFormatter->format($dateValueEnd->getTimestamp(), 'custom', 'M', NULL, $context->getContextLanguage());
        if (($startMonth == $endMonth)) {
          // Only show one month and year.
          $date_range = $this->dateFormatter->format($dateValue->getTimestamp(), 'custom', 'j.') . '–' . $this->dateFormatter->format($dateValueEnd->getTimestamp(), 'custom', 'j.', NULL, $context->getContextLanguage()) . ' ' . $endMonth . ' ' . $endYear;

          // Check the day.
          $startDay = $this->dateFormatter->format($dateValue->getTimestamp(), 'custom', 'j', NULL, $context->getContextLanguage());
          $endDay = $this->dateFormatter->format($dateValueEnd->getTimestamp(), 'custom', 'j', NULL, $context->getContextLanguage());
          if ($startDay == $endDay) {
            // Only the time differs.
            $date_range = $this->dateFormatter->format($dateValue->getTimestamp(), 'custom', 'j. M Y', NULL, $context->getContextLanguage());
          }
        }
      }
    }
    return [
      'date_range' => $date_range,
      'time_range' => $time_range,
    ];
  }
}
