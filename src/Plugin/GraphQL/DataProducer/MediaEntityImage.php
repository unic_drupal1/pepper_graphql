<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\file\Entity\File;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use GraphQL\Deferred;

/**
 * @DataProducer(
 *   id = "media_entity_image",
 *   name = @Translation("Media Entity Image"),
 *   description = @Translation("Provides Infos about Images."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "input" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *    "info" = @ContextDefinition("string",
 *       label = @Translation("Information")
 *     ),
 *    "language" = @ContextDefinition("string",
 *      label = @Translation("Context language"),
 *      required = FALSE
 *    )
 *   }
 * )
 */
class MediaEntityImage extends DataProducerPluginBase {

  /**
   * Returns specific image infos.
   *
   * @param EntityInterface $input
   *   The input media entity.
   * @param string $info
   *   The info which is seeked.
   *
   * @return \GraphQL\Deferred
   *   The found information.
   */
  public function resolve(EntityInterface $input, string $info, $language) {
    $value = '';

    // Translate media into context language.
    if (isset($language) && $input instanceof ContentEntityInterface && $input->hasTranslation($language)) {
      $input = $input->getTranslation($language);
    }

    $field = 'field_media_image';
    if (!$input->hasField('field_media_image') && $input->hasField('field_media_svg')) {
      $field = 'field_media_svg';
    }
    $values = $input->get($field)->first()->getValue();
    $file = File::load($values['target_id']);

    switch ($info) {
      case 'id':
        $value = $values['target_id'];
        break;

      case 'alt':
        $value = $values['alt'];
        break;

      case 'title':
        $value = $values['title'];
        break;

      case 'width':
        $value = $values['width'];
        break;

      case 'height':
        $value = $values['height'];
        break;

      case 'type':
        $mimetype = $file->get('filemime')->getValue();
        $value = $mimetype[0]['value'];
        break;

      case 'src':
        $uri = $file->getFileUri();
        $value = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
        break;
    }
    return new Deferred(function () use ($value) {
      return $value;
    });
  }

}
