<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\TypedData;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\TypedData\TypedDataTrait;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\typed_data\DataFetcherTrait;
use Drupal\typed_data\Exception\InvalidArgumentException;

/**
 * Resolves a typed data value at a given property path.
 *
 * @DataProducer(
 *   id = "pepper_graphql_property_path",
 *   name = @Translation("PepperGraphQL Property path"),
 *   description = @Translation("Resolves a typed data value at a given property path."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Property value")
 *   ),
 *   consumes = {
 *     "path" = @ContextDefinition("string",
 *       label = @Translation("Property path")
 *     ),
 *     "value" = @ContextDefinition("any",
 *       label = @Translation("Root value")
 *     ),
 *     "type" = @ContextDefinition("string",
 *       label = @Translation("Root type"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class PropertyPath extends DataProducerPluginBase {
  use TypedDataTrait;
  use DataFetcherTrait;

  /**
   * Resolve the property path.
   *
   * @param string $path
   *   The path to this property.
   * @param mixed $value
   *   The value the property is taken from.
   * @param string|null $type
   *   The type of the given value.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   The FieldContext.
   *
   * @return mixed|null
   */
  public function resolve($path, $value, $type, RefinableCacheableDependencyInterface $metadata) {
    if ($value instanceof TranslatableInterface && $value->isTranslatable()) {
      $languages = $value->getTranslationLanguages();
      $language = $metadata->getContextLanguage();
      if (array_key_exists($language, $languages)) {
        $value = $value->getTranslation($language);
        $value->addCacheContexts(["static:language:{$language}"]);
      }
    }

    if (!($value instanceof TypedDataInterface) && !empty($type)) {
      $manager = $this->getTypedDataManager();
      $definition = $manager->createDataDefinition($type);
      $value = $manager->create($definition, $value);
    }

    if (!($value instanceof TypedDataInterface)) {
      throw new \BadMethodCallException('Could not derive typed data type.');
    }

    $bubbleable = new BubbleableMetadata();
    $fetcher = $this->getDataFetcher();

    try {
      $output = $fetcher->fetchDataByPropertyPath($value, $path, $bubbleable, 'de')->getValue();
    }
    catch (MissingDataException $exception) {
      // There is no data at the given path.
    }
    catch (InvalidArgumentException $exception) {
      // The path is invalid for the source object.
    }
    finally {
      $metadata->addCacheableDependency($bubbleable);
    }

    return $output ?? NULL;
  }

}
