<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\TypedData;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\system\Entity\Menu;
use GraphQL\Deferred;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "pepper_graphql_menu_name",
 *   name = @Translation("Media Entity Audio"),
 *   description = @Translation("Provides the name of a menu."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "menu" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class MenuName extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupals language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupals entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * CreateArticle constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Drupals language manager.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, LanguageManagerInterface $language_manager, EntityTypeManager $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Returns specific image infos.
   *
   * @param \Drupal\system\Entity\Menu $menu
   *   The menu entity.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   The context object from drupal.
   *
   * @return \GraphQL\Deferred
   *   The translated label.
   */
  public function resolve(Menu $menu, FieldContext $context) {
    $language = $context->getContextLanguage();
    $original_language = $this->languageManager->getConfigOverrideLanguage();
    $this->languageManager->setConfigOverrideLanguage($this->languageManager->getLanguage($language));
    $translated_menu_entity = $this->entityTypeManager->getStorage('menu')->load($menu->id());
    $label = $translated_menu_entity->label();
    $this->languageManager->setConfigOverrideLanguage($original_language);

    return new Deferred(function () use ($label) {
      return $label;
    });
  }

}
