<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\LayoutParagraphs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql\GraphQL\Buffers\EntityRevisionBuffer;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use GraphQL\Deferred;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Loads the entity reference layout revisions.
 *
 * @DataProducer(
 *   id = "layout_paragraphs_regions",
 *   name = @Translation("Layout"),
 *   description = @Translation("Returns the layout of a SectionItem."),
 *   provider = "layout_paragraphs",
 *   produces = @ContextDefinition("entity",
 *     label = @Translation("Entity"),
 *     multiple = TRUE
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Parent entity")
 *     ),
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("Language"),
 *       multiple = TRUE,
 *       required = FALSE
 *     ),
 *     "bundle" = @ContextDefinition("string",
 *       label = @Translation("Entity bundle(s)"),
 *       multiple = TRUE,
 *       required = FALSE
 *     ),
 *     "access" = @ContextDefinition("boolean",
 *       label = @Translation("Check access"),
 *       required = FALSE,
 *       default_value = TRUE
 *     ),
 *     "access_user" = @ContextDefinition("entity:user",
 *       label = @Translation("User"),
 *       required = FALSE,
 *       default_value = NULL
 *     ),
 *     "access_operation" = @ContextDefinition("string",
 *       label = @Translation("Operation"),
 *       required = FALSE,
 *       default_value = "view"
 *     )
 *   }
 * )
 */
class LayoutParagraphsRegions extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity revision buffer service.
   *
   * @var \Drupal\graphql\GraphQL\Buffers\EntityRevisionBuffer
   */
  protected $entityRevisionBuffer;

  /**
   * Language id.
   *
   * @var string
   */
  protected $language;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('graphql.buffer.entity_revision')
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\graphql\GraphQL\Buffers\EntityRevisionBuffer $entityRevisionBuffer
   *   The entity revision buffer service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
    string $pluginId,
    array $pluginDefinition,
    EntityTypeManager $entityTypeManager,
    EntityRevisionBuffer $entityRevisionBuffer
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->entityRevisionBuffer = $entityRevisionBuffer;
  }

  /**
   * Resolves entity reference layout for a given paragraph entity.
   *
   * May optionally respect the entity bundles and language.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string|null $language
   *   Optional. Language to be respected for retrieved entities.
   * @param array|null $bundles
   *   Optional. List of bundles to be respected for retrieved entities.
   * @param bool $access
   *   Whether check for access or not. Default is true.
   * @param \Drupal\Core\Session\AccountInterface|null $accessUser
   *   User entity to check access for. Default is null.
   * @param string $accessOperation
   *   Operation to check access for. Default is view.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   The caching context related to the current field.
   *
   * @return \GraphQL\Deferred|null
   *   A promise that will return entities or NULL if there aren't any.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function resolve(EntityInterface $entity, ?string $language, ?array $bundles, ?bool $access, ?AccountInterface $accessUser, ?string $accessOperation, FieldContext $context): ?Deferred {
    $field_name = 'field_layout_paragraphs';

    if ($entity->getType() !== 'layout_section') {
      return NULL;
    }

    $this->language = $context->getContextLanguage();
    if ($this->language instanceof LanguageInterface) {
      $this->language = $this->language->getId();
    }
    if (empty($this->language)) {
      $this->language = $entity->language()->getId();
    }

    // If we are in preview mode, then all sibling paragraphs will get injected by the node edit form submit callback.
    // Otherwise, we will have no change to get the field from the parent entity, cause the parent could also be new and
    // not yet saved.
    // @see decoupled_preview__callback().
    if ($entity->siblings) {
      $siblings = $entity->siblings;
    }
    else {
      // No siblings set, do it the regular way for saved entities.
      $parent = $entity->getParentEntity();
      if (empty($parent)) {
        return NULL;
      }
      $item_list = $parent->get($field_name);
      $siblings = $item_list->referencedEntities();
    }

    foreach ($siblings as $paragraph) {
      // Get the behavior setting for the paragraph to check its region and parent_uuid.
      $behavior = $paragraph->getAllBehaviorSettings();
      // The region behavior is emtpty. That means that this is probably a layout paragraph.
      if (empty($behavior['layout_paragraphs']['region'])) {
        continue;
      }
      // If the
      if ($entity->uuid() === $behavior['layout_paragraphs']['parent_uuid']) {
        $sections[$behavior['layout_paragraphs']['region']] = [
          'id' => $paragraph->id(),
          'uuid' => $paragraph->uuid(),
          'name' => $behavior['layout_paragraphs']['region'],
        ];
      }
    }

    /** @var ParagraphInterface $paragraph */
    foreach ($siblings as $paragraph) {
      $behavior = $paragraph->getAllBehaviorSettings();

      $uuid = $paragraph->uuid();

      $region = $paragraph->getBehaviorSetting('layout_paragraphs', 'region');
      if (!empty($region) && $this->checkLanguageVisibility($paragraph) && $entity->uuid() === $behavior['layout_paragraphs']['parent_uuid']) {
        if ($paragraph->hasTranslation($this->language)) {
          $paragraph = $paragraph->getTranslation($this->language);
        }
        $sections[$region]['paragraphs'][] = $paragraph;
      }
    }

    return new Deferred(function () use ($sections) {
      return $sections;
    });


  }

  /**
   * Checks if the paragraph should be shown in this language.
   *
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The checked paragraph entity.
   *
   * @return bool
   *   Shown or not.
   */
  private function checkLanguageVisibility(Paragraph $paragraph) {
    // Check if this paragraph is published in the current language.
    if ($paragraph->hasTranslation($this->language)) {
      /** @var ParagraphInterface $translated_paragraph */
      $translated_paragraph = $paragraph->getTranslation($this->language);
      return $translated_paragraph->isPublished();
    }

    // Check if the field exists and is not complete empty, which would mean,
    // all languages are allowed.
    if ($paragraph->hasField('field_language_visibility') && !$paragraph->get('field_language_visibility')->isEmpty()) {
      $allowed_languages = array_column($paragraph->get('field_language_visibility')->getValue(), 'target_id');
      if (!in_array($this->language, $allowed_languages)) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
