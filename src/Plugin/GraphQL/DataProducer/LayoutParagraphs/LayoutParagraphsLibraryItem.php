<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\LayoutParagraphs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs_library\Entity\LibraryItem;
use GraphQL\Deferred;

/**
 * @DataProducer(
 *   id = "layout_paragraphs_library_item",
 *   name = @Translation("LibraryItem"),
 *   description = @Translation("Returns the referenced paragraph of a LibraryItem."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Parent entity")
 *     ),
 *   }
 * )
 */
class LayoutParagraphsLibraryItem extends DataProducerPluginBase {

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return \GraphQL\Deferred
   */
  public function resolve(EntityInterface $entity) {
    $paragraph = NULL;

    /** @var EntityReferenceFieldItemList $item_list */
    $item_list = $entity->get('field_reusable_paragraph');
    if (!empty($item_list->referencedEntities()[0])) {
      /** @var LibraryItem $test */
      $libary_item = $item_list->referencedEntities()[0];
      foreach ($libary_item->referencedEntities() as $referenced_entity) {
        if ($referenced_entity instanceof Paragraph) {
          $paragraph = $referenced_entity;
        }
      }
    }

    return new Deferred(function () use ($paragraph) {
      return $paragraph;
    });
  }

}
