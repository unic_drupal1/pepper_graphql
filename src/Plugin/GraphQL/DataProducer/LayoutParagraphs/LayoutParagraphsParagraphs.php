<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\LayoutParagraphs;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use GraphQL\Deferred;

/**
 * @DataProducer(
 *   id = "layout_paragraphs_paragraphs",
 *   name = @Translation("Subsections"),
 *   description = @Translation("Seeks section names and children."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "input" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *     "position" = @ContextDefinition("integer",
 *       label = @Translation("Position")
 *     ),
 *    "info" = @ContextDefinition("string",
 *       label = @Translation("Information")
 *     )
 *   }
 * )
 */
class LayoutParagraphsParagraphs extends DataProducerPluginBase {

  /**
   * @param array $input
   *   The input array.
   * @param int $position
   *   The position to seek.
   * @param string $info
   *   The info which is seeked.
   * @return mixed
   *   The element at the specified position.
   */
  public function resolve(array $input, $position, $info) {
    return new Deferred(function () use ($input, $info) {
      return $input[$info];
    });
  }

}
