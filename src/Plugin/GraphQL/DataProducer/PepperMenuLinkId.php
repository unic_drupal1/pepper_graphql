<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * TODO: Fix input context type.
 *
 * @DataProducer(
 *   id = "pepper_menu_link_id",
 *   name = @Translation("Menu link id"),
 *   description = @Translation("Returns the id of a menu link."),
 *   produces = @ContextDefinition("string",
 *     id = @Translation("Id")
 *   ),
 *   consumes = {
 *     "link" = @ContextDefinition("any",
 *       label = @Translation("Menu link")
 *     )
 *   }
 * )
 */
class PepperMenuLinkId extends DataProducerPluginBase {

  /**
   * @param \Drupal\Core\Menu\MenuLinkInterface $link
   *
   * @return mixed
   */
  public function resolve(MenuLinkInterface $link) {
    return $link->getPluginDefinition()['id'];
  }

}
