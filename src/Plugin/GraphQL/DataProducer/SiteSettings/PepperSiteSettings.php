<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\SiteSettings;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Gets the site name.
 *
 * @DataProducer(
 *   id = "pepper_site_settings",
 *   name = @Translation("Site name"),
 *   description = @Translation("Returns several site settings."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Site settings")
 *   ),
 *   consumes = {
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("Context language"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class PepperSiteSettings extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Maps the LanguageId to the langcode.
   *
   * @var array
   */
  protected $languageMapping = [
    'DE' => 'de',
    'EN' => 'en',
    'FR' => 'fr',
    'IT' => 'it',
  ];

  /**
   * The site settings.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $siteSettings;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * SiteSettings constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   *   The language manager service.
   */
  public function __construct(array $configuration, $pluginId, array $pluginDefinition, ConfigFactory $configFactory, LanguageManager $languageManager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->configFactory = $configFactory;
    $this->languageManager = $languageManager;
    // Load the site settings directly.
    $this->siteSettings = $this->configFactory->get('system.site');
  }

  /**
   * Resolves the site information.
   *
   * @param null|string $language
   *   The language shortcut.
   *
   * @return array|string
   *   The structure or a single site information.
   */
  public function resolve(string $language = NULL) {
    // This may be a multilingual site, so maybe we need the translated configs.
    if ($language) {
      // Save the actual language.
      $original_language = $this->languageManager->getConfigOverrideLanguage();
      // Set the requested language.
      $language = $this->languageManager->getLanguage($this->languageMapping[$language]);
      $this->languageManager->setConfigOverrideLanguage($language);
      // Load the settings in the new language.
      $this->siteSettings = $this->configFactory->get('system.site');
      // Now gather the overwritten settings.
      $siteSettings = $this->getSiteSettings();
      // Set language back.
      $this->languageManager->setConfigOverrideLanguage($original_language);
    }
    else {
      // Use the actual language.
      $siteSettings = $this->getSiteSettings();
    }
    return $siteSettings;
  }

  /**
   * Gathers all settings.
   *
   * @return array
   *   The settings.
   */
  private function getSiteSettings() {
    return [
      'site_name' => $this->getSiteName(),
      'slogan' => $this->getSiteSlogan(),
      'site_email' => $this->getSiteEmail(),
      'frontpage' => $this->getSiteFrontpage(),
      'default_403' => $this->getSiteDefault403(),
      'default_404' => $this->getSiteDefault404(),
      'privacy_page' => $this->getPrivacyPageUrl(),
    ];
  }

  /**
   * Returns the actual site name.
   *
   * @return string
   *   The site name.
   */
  private function getSiteName(): string {
    return $this->siteSettings->get('name');
  }

  /**
   * Returns the actual site slogan.
   *
   * @return string
   *   The site slogan.
   */
  private function getSiteSlogan(): string {
    return $this->siteSettings->get('slogan');
  }

  /**
   * Returns the actual sites email.
   *
   * @return string
   *   The sites email.
   */
  private function getSiteEmail(): string {
    return $this->siteSettings->get('mail');
  }

  /**
   * Returns the frontpage.
   *
   * @return string
   *   The frontpage url.
   */
  private function getSiteFrontpage(): string {
    return $this->siteSettings->get('page.front');
  }

  /**
   * Returns the default 403 page.
   *
   * @return string
   *   The default 403 page url.
   */
  private function getSiteDefault403(): string {
    return $this->siteSettings->get('page.403');
  }


  /**
   * Returns the default 403 page.
   *
   * @return string
   *   The default 403 page url.
   */
  private function getPrivacyPageUrl(): string {
    $nid = $this->siteSettings->get('pepper.privacy_page_nid');
    $language = $this->languageManager->getConfigOverrideLanguage()->getId();
    if ($nid && $language) {
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/'. $nid,  $language);
      return '/' . $language . '' . $alias;
    }
    return '';
  }


  /**
   * Returns the default 404 page.
   *
   * @return string
   *   The default 404 page url.
   */
  private function getSiteDefault404(): string {
    return $this->siteSettings->get('page.404');
  }

}
