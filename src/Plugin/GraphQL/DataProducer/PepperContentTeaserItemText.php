<?php

namespace Drupal\pepper_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\Entity\Node;

/**
 * Load Teaser information from paragraph or referenced node field.
 *
 * @DataProducer(
 *   id = "pepper_content_teaser_item_text",
 *   name = @Translation("Layout"),
 *   description = @Translation("Returns content Teaser Item field."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Entity")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity")
 *     ),
 *    "language" = @ContextDefinition("string",
 *       label = @Translation("Context language")
 *     ),
 *     "field_paragraph" = @ContextDefinition("string",
 *       label = @Translation("Field name paragraph")
 *     ),
 *     "field_node" = @ContextDefinition("string",
 *       label = @Translation("Field name node")
 *     ),
 *   }
 * )
 */
class PepperContentTeaserItemText extends DataProducerPluginBase {

  /**
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $language
   * @param string $field_paragraph
   * @param string $field_node
   */
  public function resolve(EntityInterface $entity, $language, $field_paragraph, $field_node) {

    // Get teaser value from paragraph.
    if (!empty($entity->get($field_paragraph)->getValue())) {
      return $entity->get($field_paragraph)->value;
    }

    // Load target node.
    if ($links = $entity->get('field_link')->getValue()) {
      if (!UrlHelper::isExternal($links[0]['uri'])) {
        if ($node_id = Url::fromUri($links[0]['uri'])->getRouteParameters()) {
          $node = Node::load($node_id['node']);
          // Translate node into context language.
          if ($node instanceof ContentEntityInterface && $node->hasTranslation($language)) {
            $translated_node = $node->getTranslation($language);
          }
        }
      }
    }

    // Get teaser value from target node, if node exists in context language.
    if (isset($translated_node) && $translated_node instanceof Node && $translated_node->get($field_node)) {
      return $translated_node->get($field_node)->value;
    }

    return NULL;

  }

}
