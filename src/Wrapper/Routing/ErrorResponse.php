<?php

namespace Drupal\pepper_graphql\Wrapper\Routing;

/**
 * Error response object.
 */
class ErrorResponse implements RouteResponseInterface {

  /**
   * The http status code.
   *
   * @var int
   */
  protected $code = 404;

  /**
   * Error message.
   *
   * @var string
   */
  protected $message = NULL;

  /**
   * ErrorResponse constructor.
   *
   * @param int $code
   *   Error response code.
   * @param string $message
   *   Optional error message.
   */
  public function __construct($code = NULL, $message = NULL) {
    $this->code = $code;
    $this->message = $message;
  }

  /**
   * {@inheritdoc}
   */
  public function code() {
    return $this->code;
  }

  /**
   * {@inheritdoc}
   */
  public function message() {
    return $this->message;
  }

}
