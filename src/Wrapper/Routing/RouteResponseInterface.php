<?php

namespace Drupal\pepper_graphql\Wrapper\Routing;

/**
 * Interface RouteResponseInterface.
 */
interface RouteResponseInterface {

  /**
   * Returns the status code of the response.
   *
   * @return int
   *   Status code.
   */
  public function code();

}
