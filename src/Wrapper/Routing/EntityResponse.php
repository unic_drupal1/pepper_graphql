<?php

namespace Drupal\pepper_graphql\Wrapper\Routing;

/**
 * Class EntityResponse.
 */
class EntityResponse implements RouteResponseInterface {

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The http status code.
   *
   * @var int
   */
  protected $code = 200;

  /**
   * EntityResponse constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object to return.
   */
  public function __construct($entity = NULL) {
    $this->entity = $entity;
  }

  /**
   * Returns the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity object.
   */
  public function entity() {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function code() {
    return $this->code;
  }

}
