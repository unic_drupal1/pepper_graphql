<?php

namespace Drupal\pepper_graphql\Wrapper\Routing;

/**
 * RedirectResponse object.
 */
class RedirectResponse implements RouteResponseInterface {

  /**
   * The redirect target url.
   *
   * @var string
   */
  protected $target;

  /**
   * The http status code.
   *
   * @var int
   */
  protected $code;

  /**
   * RedirectResponse constructor.
   *
   * @param string $target
   *   The redirect target.
   * @param int $code
   *   The http status code.
   */
  public function __construct($target, $code = 301) {
    $this->target = $target;
    $this->code = $code;
  }

  /**
   * Returns the redirect target url.
   *
   * @return string
   *   The redirect target url.
   */
  public function target() {
    return $this->target;
  }

  /**
   * {@inheritdoc}
   */
  public function code() {
    return $this->code;
  }

}
