<?php

namespace Drupal\Tests\pepper_graphql\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\graphql\Entity\Server;
use Drupal\graphql\Event\OperationEvent;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\graphql\Kernel\GraphQLTestBase;
use GraphQL\Deferred;
use GraphQL\Executor\Promise\Adapter\SyncPromiseAdapter;
use Prophecy\Argument;

/**
 * Test class for the PepperKernelTestBase data producer.
 *
 * @group pepper_graphql
 */
class PepperKernelTestBase extends GraphQLTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'language',
    'node',
    'graphql',
    'content_translation',
    'entity_reference_test',
    'field',
    'menu_link_content',
    'link',
    'typed_data',
    'pepper_graphql',
    'path',
    'path_alias',
    'menu_ui',
  ];

  /**
   * Removes the need to preconfigure a schema.
   *
   * See: core/lib/Drupal/Core/Config/Development/ConfigSchemaChecker.php.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('path_alias');
    $this->installEntitySchema('menu_link_content');

    ConfigurableLanguage::create([
      'id' => 'it',
      'weight' => 3,
    ])->save();

    $config = $this->config('language.negotiation');
    $config->set('url.prefixes', [
      'en' => 'en',
      'de' => 'de',
      'fr' => 'fr',
      'it' => 'it',
    ])->save();

    /** @var \Drupal\language\LanguageNegotiatorInterface $language_negotiator */
    $language_negotiator = \Drupal::getContainer()->get('language_negotiator');

    // Set Content Language Negotiator to use just the URL.
    $language_negotiator->saveConfiguration('language_interface', [
      'language-url' => 4,
      'language-selected' => 5,
    ]);

    \Drupal::service('kernel')->rebuildContainer();
    $language_negotiator->setCurrentUser(\Drupal::currentUser());
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);

    $definition = $container->getDefinition('path_alias.path_processor');
    $definition
      ->addTag('path_processor_inbound', ['priority' => 100])
      ->addTag('path_processor_outbound', ['priority' => 300]);
  }

  /**
   * Loads the pepper_graphql module graphql schema.
   */
  protected function loadModuleSchema() {
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('pepper_graphql')->getPath();
    $schema_path = $module_path . '/graphql/custom_schema.base.graphqls';
    $schema = file_get_contents($schema_path);

    $schema_id = 'custom_composable';

    $server_values = [
      'name' => 'pepper_graphql',
      'schema' => $schema_id,
      'schema_configuration' => [
        $schema_id => [
          'extensions' => [
            'custom_schema' => 'custom_schema',
            'layout_paragraphs' => 'layout_paragraphs',
            'menu' => 'menu',
            'site_settings' => 'site_settings',
            'static_routes' => 'static_routes',
          ],
        ],
      ],
      'caching' => FALSE,
      'batching' => TRUE,
      'debug_flag' => 15,
      'status' => TRUE,
    ];

    $this->setUpSchema($schema, $schema_id, $server_values);
  }

  /**
   * Loads an example file to get a query.
   *
   * @param string $module
   *   The module the example is from.
   * @param string $example
   *   The name of the example without the extension.
   *
   * @return null|string
   *   The example query or NULL.
   */
  protected function loadExampleQuery(string $module, string $example): string {
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule($module)->getPath();
    $example_path = $module_path . '/examples/' . $example . '.graphqls';
    return file_get_contents($example_path);
  }

  /**
   * This is a copy of DataProducerExecutionTrait::executeDataProducer.
   *
   * We needed to add the setContextLanguage() Method.
   *
   * @param string $id
   *   Dataproducer id.
   * @param array $contexts
   *   Contexts to set for this resolver.
   *
   * @return \GraphQL\Deferred|\GraphQL\Executor\ExecutionResult|mixed
   *   Return value.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function executeDataProducer($id, $contexts = []) {
    /** @var \Drupal\graphql\Plugin\DataProducerPluginManager $manager */
    $manager = $this->container->get('plugin.manager.graphql.data_producer');

    /** @var \Drupal\graphql\Plugin\DataProducerPluginInterface $plugin */
    $plugin = $manager->createInstance($id);
    foreach ($contexts as $key => $value) {
      $plugin->setContextValue($key, $value);
    }

    /** @var \Drupal\graphql\GraphQL\Execution\FieldContext $context */
    $context = $this->prophesize(FieldContext::class);
    $context->addCacheableDependency(Argument::any())->willReturn($context->reveal());
    $context->addCacheContexts(Argument::any())->willReturn($context->reveal());
    $context->addCacheTags(Argument::any())->willReturn($context->reveal());
    $context->mergeCacheMaxAge(Argument::any())->willReturn($context->reveal());
    $context->getContextValue(Argument::any(), Argument::any())->willReturn(NULL);
    $context->setContextValue(Argument::any(), Argument::any())->willReturn(FALSE);
    $context->hasContextValue(Argument::any())->willReturn(FALSE);
    $context->setContextLanguage(Argument::any())->willReturn($context->reveal());

    // Make sure, the resolve context returns the language, which was set by
    // setContextLanguage(). This is used by the graphql language negotiation.
    $resolveContext = $this->prophesize(ResolveContext::class);
    $resolveContext->setContextLanguage(Argument::type('string'))->will(function ($args) use ($resolveContext) {
      $resolveContext->getContextLanguage()->willReturn($args[0]);
    });
    $resolveContext->getServer()->willReturn(Server::create([
      'name' => $this->randomGenerator->name(),
    ]));


    $resolveContextMock = $resolveContext->reveal();
    if (!empty($contexts['language'])) {
      $resolveContextMock->setContextLanguage($contexts['language']);
    }
    else {
      $resolveContextMock->setContextLanguage('de');
    }

    $event = new OperationEvent($resolveContextMock);

    // Get the event_dispatcher service and dispatch the event.
    $event_dispatcher = $this->container->get('event_dispatcher');
    $event_dispatcher->dispatch($event, OperationEvent::GRAPHQL_OPERATION_BEFORE);

    $result = $plugin->resolveField($context->reveal());
    if (!$result instanceof Deferred) {
      return $result;
    }

    $adapter = new SyncPromiseAdapter();
    return $adapter->wait($adapter->convertThenable($result));
  }

}
