<?php

namespace Drupal\Tests\pepper_graphql\Kernel\Entity;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\pepper_graphql\Wrapper\Routing\EntityResponse;
use Drupal\pepper_graphql\Wrapper\Routing\ErrorResponse;
use Drupal\pepper_graphql\Wrapper\Routing\RedirectResponse;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\Tests\pepper_graphql\Kernel\PepperKernelTestBase;

/**
 * Test class for the PepperRouteResolver data producer.
 *
 * @group pepper_graphql
 */
class PepperBreadcrumbProducerTest extends PepperKernelTestBase {

  /** @var \Drupal\node\NodeInterface */
  protected $node_l1;

  /** @var \Drupal\node\NodeInterface */
  protected $node_l1_de;

  /** @var \Drupal\node\NodeInterface */
  protected $node_l1_fr;

  /** @var \Drupal\node\NodeInterface */
  protected $node_l2;

  /** @var \Drupal\node\NodeInterface */
  protected $node_l2_de;

  /** @var \Drupal\node\NodeInterface */
  protected $node_l2_fr;

  protected $menu_link_l1;

  protected $menu_link_l1_de;

  protected $menu_link_l1_fr;

  protected $menu_link_l2;

  protected $menu_link_l2_de;

  protected $menu_link_l2_fr;


  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $content_type = NodeType::create([
      'type' => 'content_page',
      'name' => 'Content page',
      'translatable' => TRUE,
      'display_submitted' => FALSE,
    ]);
    $content_type->save();

    // Published node and published translations.
    $this->node_l1 = Node::create([
      'title' => 'Test content page L1',
      'type' => 'content_page',
      'status' => NodeInterface::PUBLISHED,
    ]);
    $this->node_l1->save();

    $this->node_l1_fr = $this->node_l1->addTranslation('fr', ['title' => 'Test content page L1 FR']);
    $this->node_l1_fr->save();


    $this->node_l1_de = $this->node_l1->addTranslation('de', ['title' => 'Test content page L1 DE']);
    $this->node_l1_de->save();

    $this->menu_link_l1 = MenuLinkContent::create([
      'title' => $this->node_l1->label(),
      'link' => ['uri' => 'internal:/node/' . $this->node_l1->id()],
      'menu_name' => 'main',
      'expanded' => TRUE,
    ]);
    $this->menu_link_l1->save();

    $this->menu_link_l1_fr = $this->menu_link_l1->addTranslation('fr', ['title' => 'Test content page L1 FR']);
    $this->menu_link_l1_fr->save();

    $this->menu_link_l1_de = $this->menu_link_l1->addTranslation('de', ['title' => 'Test content page L1 DE']);
    $this->menu_link_l1_de->save();


    // Published node and published translations.
    $this->node_l2 = Node::create([
      'title' => 'Test content page L2',
      'type' => 'content_page',
      'status' => NodeInterface::PUBLISHED,
    ]);
    $this->node_l2->save();

    $this->node__l2_fr = $this->node_l2->addTranslation('fr', ['title' => 'Test content page L2 FR']);
    $this->node__l2_fr->save();

    $this->node__l2_de = $this->node_l2->addTranslation('de', ['title' => 'Test content page L2 DE']);
    $this->node__l2_de->save();

    $this->menu_link_l2 = MenuLinkContent::create([
      'title' => $this->node_l2->label(),
      'link' => ['uri' => 'internal:/node/' . $this->node_l2->id()],
      'menu_name' => 'main',
      'expanded' => TRUE,
    ]);
    $this->menu_link_l2->save();

    $this->menu_link_l2_fr = $this->menu_link_l2->addTranslation('fr', ['title' => 'Test content page L2 FR']);
    $this->menu_link_l2_fr->save();

    $this->menu_link_l2_de = $this->menu_link_l2->addTranslation('de', ['title' => 'Test content page L2 DE']);
    $this->menu_link_l2_de->save();

    \Drupal::service('content_translation.manager')->setEnabled('node', 'content_page', TRUE);
  }

  /**
   * @covers \Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\PepperRouteLoad::resolve
   */
  public function testRouteEntityResponse() {

    // Test for german nodes - fetch the entity for the url /node/1 (de)
    $result = $this->executeDataProducer('pepper_route_load', [
      'path' => sprintf('/node/%d', $this->node_l1->id()),
      'language' => 'de',
    ]);

    // Todo (SJa): Write test as soon as tests can be run on a arm m1 macbook.
    $this->assertEquals(200, $result->code());
    $this->assertEquals($this->node_l1_de->id(), $result->entity()->id());
  }

}
