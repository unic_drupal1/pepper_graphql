<?php

namespace Drupal\Tests\pepper_graphql\Kernel;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\system\Entity\Menu;

/**
 * Test class for the PepperMenuQueryTest data producer.
 *
 * @group pepper_graphql
 */
class PepperMenuQueryTest extends PepperKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {

    parent::setUp();

    $this->loadModuleSchema();

    $this->menuLinkManager = $this->container->get('plugin.manager.menu.link');

    $this->menu = Menu::create([
      'id' => 'test_menu',
      'label' => 'Test menu',
      'description' => 'Test menu',
    ]);

    $this->menu->save();

    $base_options = [
      'title' => 'Menu link test',
      'provider' => 'graphql',
      'menu_name' => 'test_menu',
    ];

    $parent = $base_options + [
      'link' => [
        'uri' => 'internal:/test-menu/hierarchy/parent',
        'options' => [
          'attributes' => [
            'target' => '_blank',
          ],
        ],
      ],
      'description' => 'Test description',
    ];

    $link = MenuLinkContent::create($parent);
    $link->save();
    $links['parent'] = $link->getPluginId();
    $this->testLink = $link;

    $child_1 = $base_options + [
      'link' => ['uri' => 'internal:/test-menu/hierarchy/parent/child'],
      'parent' => $links['parent'],
    ];
    $link = MenuLinkContent::create($child_1);
    $link->save();
    $links['child-1'] = $link->getPluginId();

    $child_1_1 = $base_options + [
      'link' => ['uri' => 'internal:/test-menu/hierarchy/parent/child2/child'],
      'parent' => $links['child-1'],
    ];
    $link = MenuLinkContent::create($child_1_1);
    $link->save();
    $links['child-1-1'] = $link->getPluginId();

    $child_1_2 = $base_options + [
      'link' => ['uri' => 'internal:/test-menu/hierarchy/parent/child2/child'],
      'parent' => $links['child-1'],
    ];
    $link = MenuLinkContent::create($child_1_2);
    $link->save();
    $links['child-1-2'] = $link->getPluginId();

    $child_2 = $base_options + [
      'link' => ['uri' => 'internal:/test-menu/hierarchy/parent/child'],
      'parent' => $links['parent'],
    ];
    $link = MenuLinkContent::create($child_2);
    $link->save();
    $links['child-2'] = $link->getPluginId();

    $this->menuLinkTree = $this->container->get('menu.link_tree');
    $this->linkTree = $this->menuLinkTree->load('test_menu', new MenuTreeParameters());

    \Drupal::service('kernel')->rebuildContainer();

  }

  /**
   * {@inheritdoc}
   */
  protected function userPermissions(): array {
    $permissions = parent::userPermissions();
    $permissions[] = 'administer menu';
    return $permissions;
  }

  /**
   * Test menu queries.
   */
  public function testMenuQueryResponse() {
    $query = $this->loadExampleQuery('pepper_graphql', 'menu.query.en');

    $response = $this->query($query);
    $content = json_decode($response->getContent(), TRUE);

    $expected = [
      'menu' => [
        'name' => 'Test menu',
        'items' => [
          0 => [
            'title' => 'Menu link test',
            'url' => [
              'path' => '/test-menu/hierarchy/parent',
            ],
          ],
        ],
      ],
    ];

    $this->assertEquals(200, $response->getStatusCode());
    $this->assertSame($expected, $content['data']);
  }

  /**
   * Test menu queries.
   */
  public function testMenuQueryResponseNonDefaultLanguage() {
    $query = $this->loadExampleQuery('pepper_graphql', 'menu.query.de');

    $response = $this->query($query);
    $content = json_decode($response->getContent(), TRUE);

    $expected = [
      'menu' => [
        'name' => 'Test menu',
        'items' => [],
      ],
    ];

    $this->assertEquals(200, $response->getStatusCode());
    $this->assertSame($expected, $content['data']);
  }

}
