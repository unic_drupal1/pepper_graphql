<?php

namespace Drupal\Tests\pepper_graphql\Kernel\Routing;

use Drupal\pepper_graphql\Wrapper\Routing\EntityResponse;
use Drupal\pepper_graphql\Wrapper\Routing\ErrorResponse;
use Drupal\pepper_graphql\Wrapper\Routing\RedirectResponse;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\Tests\pepper_graphql\Kernel\PepperKernelTestBase;

/**
 * Test class for the PepperRouteResolver data producer.
 *
 * @group pepper_graphql
 */
class PepperRouteResolverTest extends PepperKernelTestBase {

  /** @var \Drupal\node\NodeInterface */
  protected $node;

  /** @var \Drupal\node\NodeInterface */
  protected $node_de;

  /** @var \Drupal\node\NodeInterface */
  protected $node_fr;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $content_type = NodeType::create([
      'type' => 'content_page',
      'name' => 'Content page',
      'translatable' => TRUE,
      'display_submitted' => FALSE,
    ]);
    $content_type->save();

    // Published node and published translations.
    $this->node = Node::create([
      'title' => 'Test content page',
      'type' => 'content_page',
      'status' => NodeInterface::PUBLISHED,
    ]);
    $this->node->save();

    $this->node_fr = $this->node->addTranslation('fr', ['title' => 'Test content page FR']);
    $this->node_fr->save();

    $this->node_de = $this->node->addTranslation('de', ['title' => 'Test content page DE']);
    $this->node_de->save();

    \Drupal::service('content_translation.manager')->setEnabled('node', 'content_page', TRUE);

  }

  /**
   * @covers \Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\PepperRouteLoad::resolve
   */
  public function testRouteEntityResponse() {

    // Test for german nodes - fetch the entity for the url /node/1 (de)
    $result = $this->executeDataProducer('pepper_route_load', [
      'path' => sprintf('/node/%d', $this->node->id()),
      'language' => 'de',
    ]);

    $this->assertEquals(200, $result->code());
    $this->assertInstanceOf(EntityResponse::class, $result);
    $this->assertInstanceOf(NodeInterface::class, $result->entity());
    $this->assertEquals($this->node_de->id(), $result->entity()->id());
    $this->assertEquals($this->node_de->label(), $result->entity()->label());

    // Fetch the entity for the url node/1 (fr)
    $result = $this->executeDataProducer('pepper_route_load', [
      'path' => sprintf('/node/%d', $this->node->id()),
      'language' => 'fr',
    ]);

    $this->assertEquals(200, $result->code());
    $this->assertInstanceOf(EntityResponse::class, $result);
    $this->assertInstanceOf(NodeInterface::class, $result->entity());
    $this->assertEquals($this->node_fr->id(), $result->entity()->id());
    $this->assertEquals('Test content page FR', $result->entity()->label());

    // Fetch the entity for a non existent translation (it)
    $result = $this->executeDataProducer('pepper_route_load', [
      'path' => sprintf('/node/%d', $this->node->id()),
      'language' => 'it',
    ]);

    $this->assertInstanceOf(ErrorResponse::class, $result);
    $this->assertEquals(404, $result->code());
    $this->assertEquals('No translation found for it', $result->message());

    // Test for errors (missing leading slash) for the url node/1 (de)
    $result = $this->executeDataProducer('pepper_route_load', [
      'path' => sprintf('node/%d', $this->node->id()),
      'language' => 'de',
    ]);

    $this->assertInstanceOf(ErrorResponse::class, $result);
    $this->assertEquals(404, $result->code());
    $this->assertEquals('A path argument has to start with one single slash', $result->message());

    // Test that path aliases are handled correctly.
    $path_alias_de = \Drupal::entityTypeManager()->getStorage('path_alias')->create([
      'path' => '/node/' . $this->node_de->id(),
      'alias' => '/german-alias',
      'langcode' => 'de',
    ]);
    $path_alias_de->save();

    $path_alias_fr = \Drupal::entityTypeManager()->getStorage('path_alias')->create([
      'path' => '/node/' . $this->node_de->id(),
      'alias' => '/alias-francais',
      'langcode' => 'fr',
    ]);
    $path_alias_fr->save();

    $result = $this->executeDataProducer('pepper_route_load', [
      'path' => '/german-alias',
      'language' => 'de',
    ]);

    $this->assertEquals(200, $result->code());
    $this->assertInstanceOf(EntityResponse::class, $result);
    $this->assertInstanceOf(NodeInterface::class, $result->entity());
    $this->assertEquals($this->node_de->id(), $result->entity()->id());
    $this->assertEquals($this->node_de->label(), $result->entity()->label());

    $result = $this->executeDataProducer('pepper_route_load', [
      'path' => '/alias-francais',
      'language' => 'fr',
    ]);

    $this->assertEquals(200, $result->code());
    $this->assertInstanceOf(EntityResponse::class, $result);
    $this->assertInstanceOf(NodeInterface::class, $result->entity());
    $this->assertEquals($this->node_fr->id(), $result->entity()->id());
    $this->assertEquals($this->node_fr->label(), $result->entity()->label());

    // @TODO Skip the RedirectResponse test for this POC.
    // See: modules/custom/pepper_graphql/src/Plugin/GraphQL/DataProducer/PepperRouteLoad.php:222
//    $result = $this->executeDataProducer('pepper_route_load', [
//      'path' => sprintf('/node/%d', $this->node->id()),
//      'language' => 'fr',
//    ]);
//
//    $this->assertInstanceOf(RedirectResponse::class, $result);
//    $this->assertEquals(301, $result->code());
//    $this->assertEquals('/fr/alias-francais', $result->target());

  }

}
