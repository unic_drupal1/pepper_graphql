<?php

namespace Drupal\Tests\pepper_graphql\Kernel\Routing;

use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\pepper_graphql\Wrapper\Routing\EntityResponse;
use Drupal\Tests\pepper_graphql\Kernel\PepperKernelTestBase;

/**
 * Test class for the PepperAlternatesResolverTest data producer.
 *
 * @group pepper_graphql
 */
class PepperAlternatesResolverTest extends PepperKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {

    parent::setUp();

    $this->loadModuleSchema();

    $content_type = NodeType::create([
      'type' => 'content_page',
      'name' => 'Content page',
      'translatable' => TRUE,
      'display_submitted' => FALSE,
    ]);
    $content_type->save();

    // Published node and published translations.
    $this->node = Node::create([
      'title' => 'Test content page',
      'type' => 'content_page',
      'status' => NodeInterface::PUBLISHED,
    ]);
    $this->node->save();

    $this->node_fr = $this->node->addTranslation('fr', ['title' => 'Test content page FR']);
    $this->node_fr->save();

    $this->node_de = $this->node->addTranslation('de', ['title' => 'Test content page DE']);
    $this->node_de->save();

    \Drupal::service('content_translation.manager')->setEnabled('node', 'content_page', TRUE);

    \Drupal::service('kernel')->rebuildContainer();

  }

  /**
   * @covers \Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\PepperRouteLoad::resolve
   */
  public function testAlternatesResponse() {
    $result = $this->executeDataProducer('pepper_route_load', [
      'path' => '/node/1',
      'language' => 'de',
    ]);

    $this->assertInstanceOf(EntityResponse::class, $result);
    $this->assertInstanceOf(NodeInterface::class, $result->entity());

    $query = $this->loadExampleQuery('pepper_graphql', 'alternates.query');

    $response = $this->query($query);
    $content = json_decode($response->getContent(), TRUE);

    $expected = [
      'alternates' => [
        [
          'language_id' => 'EN',
          'url' => '/en/node/1',
        ],
        [
          'language_id' => 'FR',
          'url' => '/fr/node/1',
        ],
        [
          'language_id' => 'DE',
          'url' => '/de/node/1',
        ],
      ],
      'nid' => 1,
      'title' => 'Test content page DE',
    ];

    $this->assertEquals(200, $response->getStatusCode());
    $this->assertSame($expected, $content['data']['router']['entity']);
  }

}
