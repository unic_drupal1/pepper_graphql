<?php

namespace Drupal\Tests\pepper_graphql\Kernel\SiteSettings;

use Drupal\Tests\pepper_graphql\Kernel\PepperKernelTestBase;

/**
 * Test class for the PepperSiteSettings data producer.
 *
 * @group pepper_graphql
 */
class PepperSiteSettingsTest extends PepperKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('system.site');
    $config->set('name', 'Unic Pepper');
    $config->set('slogan', 'The Drupal Open source Headless CMS');
    $config->set('mail', 'drupal@unic.com');
    $config->save(TRUE);

    // Create a french translation of this config.
    \Drupal::languageManager()
      ->getLanguageConfigOverride('fr', 'system.site')
      ->set('name', 'Unic Poivre')
      ->set('slogan', 'Le CMS sans tête Drupal Open source')
      ->set('mail', 'drupal-fr@unic.com')
      ->save();

  }

  /**
   * @covers \Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\SiteSettings\PepperSiteSettings::resolve
   */
  public function testSiteSettings() {

    // Test the config without an language argument.
    $result = $this->executeDataProducer('pepper_site_settings');
    $this->assertEquals('Unic Pepper', $result['site_name']);
    $this->assertEquals('The Drupal Open source Headless CMS', $result['slogan']);
    $this->assertEquals('drupal@unic.com', $result['site_email']);

    // Test for the german config.
    $result = $this->executeDataProducer('pepper_site_settings', [
      'language' => 'DE',
    ]);
    $this->assertEquals('Unic Pepper', $result['site_name']);
    $this->assertEquals('The Drupal Open source Headless CMS', $result['slogan']);
    $this->assertEquals('drupal@unic.com', $result['site_email']);

    // Test for the french config.
    $result = $this->executeDataProducer('pepper_site_settings', [
      'language' => 'FR',
    ]);
    $this->assertEquals('Unic Poivre', $result['site_name']);
    $this->assertEquals('Le CMS sans tête Drupal Open source', $result['slogan']);
    $this->assertEquals('drupal-fr@unic.com', $result['site_email']);

  }

}
